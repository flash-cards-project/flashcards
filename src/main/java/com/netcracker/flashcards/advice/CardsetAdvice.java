package com.netcracker.flashcards.advice;

import com.netcracker.flashcards.annotation.CardsetExceptionHandler;
import com.netcracker.flashcards.dto.ErrorDTO;
import com.netcracker.flashcards.exception.CardsetAlreadyHasCardWithQuestionException;
import com.netcracker.flashcards.exception.CardsetNotFoundException;
import com.netcracker.flashcards.exception.UserAlreadyHasCardsetException;
import com.netcracker.flashcards.exception.UserForCardsetManipNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(annotations = CardsetExceptionHandler.class)
public class CardsetAdvice extends AbstractAdvice {

    @ExceptionHandler(UserForCardsetManipNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleUserForCardsetManipNotFoundException(UserForCardsetManipNotFoundException ex) {
        return createErrorResponse(ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(CardsetNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleCardsetNotFoundException(CardsetNotFoundException ex) {
        return createErrorResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CardsetAlreadyHasCardWithQuestionException.class)
    public ResponseEntity<ErrorDTO> handleCardsetAlreadyHasCardWithQuestionException(CardsetAlreadyHasCardWithQuestionException ex) {
        return createErrorResponse(ex.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(UserAlreadyHasCardsetException.class)
    public ResponseEntity<ErrorDTO> handleUserAlreadyHasCardsetException(UserAlreadyHasCardsetException ex) {
        return createErrorResponse(ex.getMessage(), HttpStatus.CONFLICT);
    }
}
