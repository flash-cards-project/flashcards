package com.netcracker.flashcards.advice;

import com.netcracker.flashcards.dto.ErrorDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Set;

public abstract class AbstractAdvice {
    ResponseEntity<ErrorDTO> createErrorResponse(String message, HttpStatus httpStatus) {
        return new ResponseEntity<>(new ErrorDTO(message), httpStatus);
    }

    ResponseEntity<ErrorDTO> createErrorResponse(Set<String> messages, HttpStatus httpStatus) {
        return new ResponseEntity<>(new ErrorDTO(messages), httpStatus);
    }
}
