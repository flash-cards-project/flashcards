package com.netcracker.flashcards.advice;

import com.netcracker.flashcards.annotation.RegisterExceptionHandler;
import com.netcracker.flashcards.dto.ErrorDTO;
import com.netcracker.flashcards.exception.UserAlreadyRegistered;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(annotations = RegisterExceptionHandler.class)
public class AuthAdvice extends AbstractAdvice {

    @ExceptionHandler(UserAlreadyRegistered.class)
    public ResponseEntity<ErrorDTO> handleUserAlreadyRegisteredException(UserAlreadyRegistered ex) {
        return createErrorResponse(ex.getMessage(), HttpStatus.CONFLICT);
    }
}