package com.netcracker.flashcards.advice;

import com.netcracker.flashcards.dto.ErrorDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.Set;

@ControllerAdvice
public class GlobalAdvice extends AbstractAdvice {
    @ExceptionHandler(BindException.class)
    public ResponseEntity<ErrorDTO> handleBindException(BindException ex) {
        Set<String> errors = new HashSet<>();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            errors.add(fieldError.getDefaultMessage());
        }
        return createErrorResponse(errors, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorDTO> handleConstraintViolationException(ConstraintViolationException ex) {
        return createErrorResponse(ex.getMessage(), HttpStatus.CONFLICT);
    }
}
