package com.netcracker.flashcards.advice;

import com.netcracker.flashcards.annotation.CardExceptionHandler;
import com.netcracker.flashcards.dto.ErrorDTO;
import com.netcracker.flashcards.exception.CardForCardsetNotFoundException;
import com.netcracker.flashcards.exception.CardNotFoundException;
import com.netcracker.flashcards.exception.CardsetAlreadyHasCardWithQuestionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(annotations = CardExceptionHandler.class)
public class CardAdvice extends AbstractAdvice {
    @ExceptionHandler(CardForCardsetNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleCardForCardsetNotFoundException(CardForCardsetNotFoundException ex) {
        return createErrorResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CardNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleCardNotFoundException(CardNotFoundException ex) {
        return createErrorResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CardsetAlreadyHasCardWithQuestionException.class)
    public ResponseEntity<ErrorDTO> handleException(CardsetAlreadyHasCardWithQuestionException ex) {
        return createErrorResponse(ex.getMessage(), HttpStatus.CONFLICT);
    }
}
