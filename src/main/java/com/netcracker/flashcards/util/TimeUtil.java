package com.netcracker.flashcards.util;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TimeUtil {
    public static ZonedDateTime nowUtcZoned() {
        return Instant.now().atZone(ZoneId.of("+0"));
    }
}
