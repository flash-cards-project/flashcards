package com.netcracker.flashcards.util.converter;

import org.jetbrains.annotations.NotNull;
import org.springframework.core.convert.converter.Converter;

public class StringToLongConverter implements Converter<String, Long> {
    @Override
    public Long convert(@NotNull String source) {
        try {
            return Long.parseLong(source);
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }
}
