package com.netcracker.flashcards.util.converter;

import org.jetbrains.annotations.NotNull;
import org.springframework.core.convert.converter.Converter;

public class StringToIntegerConverter implements Converter<String, Integer> {
    @Override
    public Integer convert(@NotNull String source) {
        try {
            return Integer.parseInt(source);
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }
}
