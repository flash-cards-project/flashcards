package com.netcracker.flashcards.util.converter;

import com.netcracker.flashcards.util.constants.CardConstants.LEVEL_OF_KNOWLEDGE;

import org.springframework.core.convert.converter.Converter;

import javax.validation.constraints.NotNull;
import java.util.Locale;

public class StringToLevelOfKnowledgeEnumConverter implements Converter<String, LEVEL_OF_KNOWLEDGE> {
    @Override
    public LEVEL_OF_KNOWLEDGE convert(@NotNull String source) {
        try {
            return LEVEL_OF_KNOWLEDGE.valueOf(source.toUpperCase(Locale.ROOT));
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }
}
