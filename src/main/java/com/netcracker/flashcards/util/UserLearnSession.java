package com.netcracker.flashcards.util;

import com.netcracker.flashcards.exception.UserLearnSessionException;
import com.netcracker.flashcards.util.constants.CardConstants;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
public class UserLearnSession {
    private ZonedDateTime startTime = TimeUtil.nowUtcZoned();
    private Map<CardConstants.LEVEL_OF_KNOWLEDGE, Integer> levelCounters = new HashMap<>();
    private ZonedDateTime lastLogTime;

    public void logLevel(CardConstants.LEVEL_OF_KNOWLEDGE levelOfKnowledge) {
        if (levelOfKnowledge != CardConstants.LEVEL_OF_KNOWLEDGE.UNDEFINED) {
            int value = levelCounters.getOrDefault(levelOfKnowledge, 0);
            value++;
            levelCounters.put(levelOfKnowledge, value);

            lastLogTime = TimeUtil.nowUtcZoned();
        } else {
            throw new UserLearnSessionException("logged level must not be undefined");
        }
    }

    public int getCountOfLevel(CardConstants.LEVEL_OF_KNOWLEDGE levelOfKnowledge) {
        return levelCounters.getOrDefault(levelOfKnowledge, 0);
    }
}
