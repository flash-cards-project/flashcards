package com.netcracker.flashcards.util.constants;

public final class AuthConstants {
    // Auth's username constants
    public static final int MIN_USERNAME_LENGTH = 4;
    public static final int MAX_USERNAME_LENGTH = 32;
    public static final String USERNAME_PATTERN = "^[a-zA-Z0-9]*$";
    public static final String USERNAME_PATTERN_VALID_MSG = "The username must consist only of latin" +
            " letters and digits.";

    // Auth's password constants
    public static final int MIN_PASSWORD_LENGTH = 4;
    public static final int MAX_PASSWORD_LENGTH = 32;
    public static final String PASSWORD_PATTERN = "^[a-zA-Z0-9?!@#$%^&+=_-]*$";
    public static final String PASSWORD_PATTERN_VALID_MSG = "The password must consist only of latin" +
            " letters, digits, and ?! @ # $ % ^ & + = _ - ";
}
