package com.netcracker.flashcards.util.constants;

public final class CardsetConstants {
    // Cardset's complexity constants
    public static final int MIN_COMPLEXITY = 1;
    public static final int MAX_COMPLEXITY = 5;
    public static final int INIT_COMPLEXITY = 2;

    // Cardset's name constants
    public static final int MIN_NAME_LENGTH = 1;
    public static final int MAX_NAME_LENGTH = 32;

    // Learn session timeout in milliseconds
    public static final int LEARN_SESSION_TIMEOUT = 10 * 60 * 1000;

    // Expired learn session worker delay in milliseconds
    public static final int SESSION_SAVER_DELAY = 3 * 60 * 1000;
}
