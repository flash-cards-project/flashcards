package com.netcracker.flashcards.util.constants;

public final class CardConstants {
    // Card's tier constants
    public static final int MIN_TIER = 1;
    public static final int MAX_TIER = 5;

    // Card's question and answer constants
    public static final int MIN_FIELD_LENGTH = 1;
    public static final int MAX_FIELD_LENGTH = 255;

    // Card's level of knowledge enum
    public enum LEVEL_OF_KNOWLEDGE {
        UNDEFINED,
        GOOD,
        BAD
    }
}
