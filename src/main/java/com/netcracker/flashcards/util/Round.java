package com.netcracker.flashcards.util;

import java.util.Collections;
import java.util.Stack;

public class Round extends Stack<Long> {
    public void shuffle() {
        Collections.shuffle(this);
    }
}
