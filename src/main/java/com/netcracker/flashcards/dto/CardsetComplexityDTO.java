package com.netcracker.flashcards.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import static com.netcracker.flashcards.util.constants.CardsetConstants.MIN_COMPLEXITY;
import static com.netcracker.flashcards.util.constants.CardsetConstants.MAX_COMPLEXITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardsetComplexityDTO {
    @NotNull(message = "The complexity of cardset is incorrect or missing.")
    @Min(value = MIN_COMPLEXITY, message = "The complexity of cardset should greater than {value}.")
    @Max(value = MAX_COMPLEXITY, message = "The complexity of cardset should be less than {value}.")
    private Integer complexity;
}
