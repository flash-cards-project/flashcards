package com.netcracker.flashcards.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class CardsetLearnSessionListDTO extends CardsetIdDTO {
    private Set<CardsetLearnSessionDTO> learnSessions;

    public CardsetLearnSessionListDTO(long cardsetId, Set<CardsetLearnSessionDTO> learnSessions) {
        super(cardsetId);
        this.learnSessions = learnSessions;
    }
}
