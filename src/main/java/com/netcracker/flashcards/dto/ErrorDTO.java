package com.netcracker.flashcards.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDTO {
    private Set<String> errors;

    public ErrorDTO(HashSet<String> hashSet) {
        this.errors = hashSet;
    }

    public ErrorDTO(String error) {
        this.errors = new HashSet<>(Collections.singleton(error));
    }
}