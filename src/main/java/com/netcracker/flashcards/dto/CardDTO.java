package com.netcracker.flashcards.dto;

import com.netcracker.flashcards.entity.Card;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import static com.netcracker.flashcards.util.constants.CardConstants.MIN_TIER;
import static com.netcracker.flashcards.util.constants.CardConstants.MAX_TIER;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CardDTO extends CardsetCardQADTO {
    @Min(value = 1, message = "The card ID should be greater than 0.")
    private long id;

    @Min(value = MIN_TIER, message = "The level of knowledge of this card should be greater than " +
            "{value}.")
    @Max(value = MAX_TIER, message = "The level of knowledge of this card should be less than " +
            "{value}.")
    private int tier;

    public CardDTO(Card card) {
        super (card.getCardset().getId(), card.getQuestion(), card.getAnswer());
        this.id = card.getId();
        this.tier = card.getTier();
    }
}
