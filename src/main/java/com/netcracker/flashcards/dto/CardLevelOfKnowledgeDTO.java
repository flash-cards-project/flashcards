package com.netcracker.flashcards.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.netcracker.flashcards.util.constants.CardConstants.LEVEL_OF_KNOWLEDGE;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardLevelOfKnowledgeDTO {
    @NotNull(message = "The card ID is incorrect or missing.")
    @Min(value = 0, message = "The id of card should be greater than {value}.")
    private Long cardId;

    @NotNull(message = "The card level of knowledge is incorrect or missing.")
    private LEVEL_OF_KNOWLEDGE level;
}
