package com.netcracker.flashcards.dto;

import com.netcracker.flashcards.entity.CardsetLearnStat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = true)
public class CardsetLearnStatsDTO extends CardsetIdDTO {
    private Map<ZonedDateTime, Integer> stats;

    public CardsetLearnStatsDTO(@NonNull List<CardsetLearnStat> learnStats) {
        super(1L); //stub
        if (checkStatsHasOneCardset(learnStats)) {
            setCardsetId(learnStats.get(0).getCardset().getId());
        } else {
            throw new IllegalArgumentException("Cardset stats is incorrect or missing");
        }
        stats = learnStats.stream()
                .collect(Collectors.toMap(CardsetLearnStat::getTime, CardsetLearnStat::getProgressPoint));
    }

    private boolean checkStatsHasOneCardset(@NonNull List<CardsetLearnStat> learnStats) {
        if (learnStats.size() == 0) {
            return true;
        }
        long id = learnStats.get(0).getCardset().getId();
        for (CardsetLearnStat stat : learnStats) {
            if (stat.getCardset().getId() != id) {
                return false;
            }
        }
        return true;
    }
}
