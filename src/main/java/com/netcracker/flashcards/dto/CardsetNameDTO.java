package com.netcracker.flashcards.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.netcracker.flashcards.util.constants.CardsetConstants.MIN_NAME_LENGTH;
import static com.netcracker.flashcards.util.constants.CardsetConstants.MAX_NAME_LENGTH;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardsetNameDTO {
    @NotNull(message = "The name of cardset is missing.")
    @Size(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH,
            message = "The title of cardset must be between {min} and {max} characters long.")
    String name;
}
