package com.netcracker.flashcards.dto;

import com.netcracker.flashcards.entity.Cardset;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.netcracker.flashcards.util.constants.CardsetConstants.MIN_COMPLEXITY;
import static com.netcracker.flashcards.util.constants.CardsetConstants.MAX_COMPLEXITY;
import static com.netcracker.flashcards.util.constants.CardsetConstants.MIN_NAME_LENGTH;
import static com.netcracker.flashcards.util.constants.CardsetConstants.MAX_NAME_LENGTH;

@Data
@AllArgsConstructor
public class CardsetDTO {
    @Min(value = 1, message = "The id of cardset should be greater than {value}.")
    private long id;

    @NotNull(message = "The title of cardset is incorrect or missing.")
    @Size(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH,
            message = "The title of cardset must be between {min} and {max} characters long.")
    private String name;

    @Min(value = 0, message = "The number of cards in cardset should be greater than {value}.")
    private int numOfCards;

    @Min(value = MIN_COMPLEXITY, message = "The complexity of cardset should not be less than {value}.")
    @Max(value = MAX_COMPLEXITY, message = "The complexity of cardset should not be greater than {value}.")
    private int complexity;

    @Setter(AccessLevel.NONE)
    private int progress;

    public CardsetDTO(Cardset cardset) {
        this.id = cardset.getId();
        this.name = cardset.getName();
        this.numOfCards = cardset.getCards().size();
        this.complexity = cardset.getComplexity();
        this.progress = cardset.getProgress();
    }
}
