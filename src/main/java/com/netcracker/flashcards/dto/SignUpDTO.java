package com.netcracker.flashcards.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.netcracker.flashcards.util.constants.AuthConstants.MIN_USERNAME_LENGTH;
import static com.netcracker.flashcards.util.constants.AuthConstants.MAX_USERNAME_LENGTH;
import static com.netcracker.flashcards.util.constants.AuthConstants.MIN_PASSWORD_LENGTH;
import static com.netcracker.flashcards.util.constants.AuthConstants.MAX_PASSWORD_LENGTH;
import static com.netcracker.flashcards.util.constants.AuthConstants.PASSWORD_PATTERN;
import static com.netcracker.flashcards.util.constants.AuthConstants.PASSWORD_PATTERN_VALID_MSG;
import static com.netcracker.flashcards.util.constants.AuthConstants.USERNAME_PATTERN;
import static com.netcracker.flashcards.util.constants.AuthConstants.USERNAME_PATTERN_VALID_MSG;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUpDTO {
    @NotNull(message = "The username is incorrect or missing.")
    @Size(min = MIN_USERNAME_LENGTH, max = MAX_USERNAME_LENGTH,
            message = "The username must be between {min} and {max} characters long.")
    @Pattern(regexp = USERNAME_PATTERN,
            message = USERNAME_PATTERN_VALID_MSG)
    private String username;

    @NotNull(message = "The password is incorrect or missing.")
    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH,
            message = "The password must be between {min} and {max} characters long.")
    @Pattern(regexp = PASSWORD_PATTERN,
            message = PASSWORD_PATTERN_VALID_MSG)
    private String password;
}
