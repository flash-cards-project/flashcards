package com.netcracker.flashcards.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardsetIdDTO {
    @NotNull(message = "The cardset ID is incorrect or missing.")
    @Min(value = 1, message = "The minimum value of the cardset ID is {value}.")
    private Long cardsetId;
}
