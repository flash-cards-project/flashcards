package com.netcracker.flashcards.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.netcracker.flashcards.util.constants.CardsetConstants.MAX_NAME_LENGTH;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardsetsSearchDTO {
    @NotNull(message = "No query for cardset searching.")
    @Size(max = MAX_NAME_LENGTH, message = "The query length should be less than {max} characters" +
            " long.")
    private String query;
}
