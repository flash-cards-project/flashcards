package com.netcracker.flashcards.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.netcracker.flashcards.util.constants.CardConstants.MIN_FIELD_LENGTH;
import static com.netcracker.flashcards.util.constants.CardConstants.MAX_FIELD_LENGTH;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardsetCardQADTO extends CardsetIdDTO {
    @NotNull(message = "The card's question is missing.")
    @Size(min = MIN_FIELD_LENGTH, max = MAX_FIELD_LENGTH,
            message = "The question of card must be between {min} and {max} characters long.")
    private String question;

    @NotNull(message = "The card's answer is missing.")
    @Size(min = MIN_FIELD_LENGTH, max = MAX_FIELD_LENGTH,
            message = "The answer of card must be between {min} and {max} characters long.")
    private String answer;

    public CardsetCardQADTO(Long cardsetId, String question, String answer) {
        super(cardsetId);
        this.question = question;
        this.answer = answer;
    }
}
