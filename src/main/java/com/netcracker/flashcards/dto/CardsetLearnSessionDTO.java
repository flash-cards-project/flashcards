package com.netcracker.flashcards.dto;

import com.netcracker.flashcards.entity.CardsetLearnSession;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class CardsetLearnSessionDTO {
    private ZonedDateTime startTime;
    private ZonedDateTime stopTime;
    private int goodCount;
    private int badCount;

    public CardsetLearnSessionDTO(CardsetLearnSession cardsetLearnSession) {
        this.startTime = cardsetLearnSession.getStartTime();
        this.stopTime = cardsetLearnSession.getStopTime();
        this.goodCount = cardsetLearnSession.getGoodCount();
        this.badCount = cardsetLearnSession.getBadCount();
    }
}
