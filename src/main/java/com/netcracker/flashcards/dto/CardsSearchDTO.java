package com.netcracker.flashcards.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.netcracker.flashcards.util.constants.CardConstants.MAX_TIER;
import static com.netcracker.flashcards.util.constants.CardConstants.MAX_FIELD_LENGTH;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardsSearchDTO extends CardsetIdDTO {
    @NotNull(message = "The query for card searching is missing.")
    @Size(max = MAX_FIELD_LENGTH, message = "The query length should be less than {max} " +
            "characters long.")
    private String query;

    @NotNull(message = "The level of knowledge for card searching is incorrect or missing.")
    // 0 -> for searching any level cards.
    @Min(value = 0,
            message = "The level of knowledge of the card should be greater than {value}.")
    @Max(value = MAX_TIER,
            message = "The level of knowledge of the card should be less than {value}.")
    private Integer level;
}
