package com.netcracker.flashcards.controller;

import com.netcracker.flashcards.annotation.CardExceptionHandler;
import com.netcracker.flashcards.dto.CardDTO;
import com.netcracker.flashcards.dto.CardsSearchDTO;
import com.netcracker.flashcards.dto.CardsetCardQADTO;
import com.netcracker.flashcards.dto.CardsetIdDTO;
import com.netcracker.flashcards.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Set;

@RestController
@CardExceptionHandler
@RequestMapping("/api")
public class CardController {
    @Autowired
    private CardService cardService;

    @GetMapping("/cards")
    @PreAuthorize("@webSecurityConfig.checkCardsetOwner(authentication, #cardsetIdDTO.cardsetId)")
    public ResponseEntity<Set<CardDTO>> listOfCards(@Valid CardsetIdDTO cardsetIdDTO) {
        Set<CardDTO> cards = cardService.getListOfCards(cardsetIdDTO);
        return new ResponseEntity<>(cards, HttpStatus.OK);
    }

    @PostMapping("/cards")
    public ResponseEntity<CardDTO> addCard(@RequestBody @Valid CardsetCardQADTO cardsetCardQADTO) {
        CardDTO card = cardService.addNewCard(cardsetCardQADTO);
        return new ResponseEntity<>(card, HttpStatus.OK);
    }

    @DeleteMapping("/cards/{id:\\d+}")
    public ResponseEntity<HttpStatus> deleteCard(
            @RequestBody @Valid CardsetIdDTO cardsetIdDTO,
            @PathVariable long id) {
        cardService.deleteCardsetCard(cardsetIdDTO, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/cards/{id:\\d+}")
    public ResponseEntity<CardDTO> renameCard(
            @RequestBody @Valid CardsetCardQADTO cardsetCardQADTO,
            @PathVariable long id) {
        CardDTO card = cardService.renameCard(cardsetCardQADTO, id);
        return new ResponseEntity<>(card, HttpStatus.OK);
    }

    @GetMapping("/cards/search")
    @PreAuthorize("@webSecurityConfig.checkCardsetOwner(authentication, #cardsSearchDTO.cardsetId)")
    public ResponseEntity<Set<CardDTO>> queryListOfCards(@Valid CardsSearchDTO cardsSearchDTO) {
        Set<CardDTO> cards = cardService.getListOfCardsByQuery(cardsSearchDTO);
        return new ResponseEntity<>(cards, HttpStatus.OK);
    }
}
