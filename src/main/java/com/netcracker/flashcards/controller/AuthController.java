package com.netcracker.flashcards.controller;

import com.netcracker.flashcards.annotation.RegisterExceptionHandler;
import com.netcracker.flashcards.dto.SignUpDTO;
import com.netcracker.flashcards.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RegisterExceptionHandler
@RequestMapping("/api")
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public void register(@Valid @RequestBody SignUpDTO signUpDto) {
        userService.signUp(signUpDto);
    }

    @GetMapping("/login")
    public void loginPage() {}
}