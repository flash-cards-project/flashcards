package com.netcracker.flashcards.controller;

import com.netcracker.flashcards.annotation.CardsetExceptionHandler;
import com.netcracker.flashcards.dto.CardDTO;
import com.netcracker.flashcards.dto.CardsetComplexityDTO;
import com.netcracker.flashcards.dto.CardsetDTO;
import com.netcracker.flashcards.dto.CardsetLearnSessionListDTO;
import com.netcracker.flashcards.dto.CardsetLearnStatsDTO;
import com.netcracker.flashcards.dto.CardsetNameDTO;
import com.netcracker.flashcards.dto.CardLevelOfKnowledgeDTO;
import com.netcracker.flashcards.dto.CardsetsSearchDTO;
import com.netcracker.flashcards.entity.Cardset;
import com.netcracker.flashcards.service.CardsetLearnSessionService;
import com.netcracker.flashcards.service.CardsetService;
import com.netcracker.flashcards.util.Round;
import com.netcracker.flashcards.util.constants.CardConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Set;

@RestController
@CardsetExceptionHandler
@RequestMapping("/api")
public class CardsetController {

    @Autowired
    private CardsetService cardsetService;

    @Autowired
    private CardsetLearnSessionService cardsetLearnSessionService;

    @GetMapping("/cardsets")
    public ResponseEntity<Set<CardsetDTO>> listOfCardsets(Principal principal) {
        Set<CardsetDTO> cardsets = cardsetService.getListOfCardsets(principal);
        return new ResponseEntity<>(cardsets, HttpStatus.OK);
    }

    @PostMapping("/cardsets")
    public ResponseEntity<CardsetDTO> addCardset(Principal principal) {
        CardsetDTO cardset = cardsetService.addNewCardset(principal);
        return new ResponseEntity<>(cardset, HttpStatus.OK);
    }

    @GetMapping("/cardsets/{id:\\d+}")
    public ResponseEntity<CardsetDTO> getCardset(@PathVariable long id) {
        Cardset cardset = cardsetService.getCardsetById(id);
        return new ResponseEntity<>(new CardsetDTO(cardset), HttpStatus.OK);
    }

    @DeleteMapping("/cardsets/{id:\\d+}")
    public ResponseEntity<HttpStatus> deleteCardset(@PathVariable long id) {
        cardsetService.deleteCardset(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/cardsets/{id:\\d+}")
    public ResponseEntity<CardsetDTO> renameCardset(
            @PathVariable long id,
            @RequestBody @Valid CardsetNameDTO cardsetNameDTO,
            Principal principal) {
        CardsetDTO renamedCardset = cardsetService.renameCardset(id, cardsetNameDTO, principal);
        return new ResponseEntity<>(renamedCardset, HttpStatus.OK);
    }

    @PutMapping("/cardsets/{id:\\d+}/complexity")
    public ResponseEntity<CardsetDTO> changeCardsetComplexity(
            @PathVariable long id,
            @RequestBody @Valid CardsetComplexityDTO cardsetComplexityDTO) {
        CardsetDTO cardsetDTO = cardsetService.changeCardsetComplexity(id, cardsetComplexityDTO);
        return new ResponseEntity<>(cardsetDTO, HttpStatus.OK);
    }

    @GetMapping("/cardsets/{id:\\d+}/nextcard")
    public ResponseEntity<CardDTO> getNextCard(
            @PathVariable long id, @Valid CardLevelOfKnowledgeDTO cardLevelOfKnowledgeDTO,
            HttpSession httpSession) {
        cardsetLearnSessionService.logLevel(id, cardLevelOfKnowledgeDTO.getLevel());

        String roundIdent = id + "-round";
        Round round = (Round) httpSession.getAttribute(roundIdent);
        if (round == null) {
            round = new Round();
            httpSession.setAttribute(roundIdent, round);
        }

        CardDTO card = cardsetService.getNextCard(id, cardLevelOfKnowledgeDTO, round);
        return new ResponseEntity<>(card, HttpStatus.OK);
    }

    @GetMapping("/cardsets/{id:\\d+}/learn_stats")
    public ResponseEntity<CardsetLearnStatsDTO> getLearnStats(@PathVariable long id) {
        return new ResponseEntity<>(cardsetService.getLearnStats(id), HttpStatus.OK);
    }

    @GetMapping("/cardsets/{id:\\d+}/learn_sessions")
    public ResponseEntity<CardsetLearnSessionListDTO> getLearnSessions(@PathVariable long id) {
        if (cardsetLearnSessionService.isSessionExisted(id)) {
            cardsetLearnSessionService.stopLearnSession(id);
        }
        return new ResponseEntity<>(cardsetLearnSessionService.getLearnSessionList(id), HttpStatus.OK);
    }

    @GetMapping("/cardsets/{id:\\d+}/stop_learn_session")
    public ResponseEntity<String> stopLearnSession(@PathVariable long id) {
        cardsetLearnSessionService.stopLearnSession(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/cardsets/search")
    public ResponseEntity<Set<CardsetDTO>> queryListOfCardsets(
            @Valid CardsetsSearchDTO cardsetsSearchDTO, Principal principal) {
        Set<CardsetDTO> cardsets = cardsetService.getListOfCardsetsByQuery(cardsetsSearchDTO, principal);
        return new ResponseEntity<>(cardsets, HttpStatus.OK);
    }
}
