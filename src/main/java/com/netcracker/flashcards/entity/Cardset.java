package com.netcracker.flashcards.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

import static com.netcracker.flashcards.util.constants.CardsetConstants.INIT_COMPLEXITY;

@Entity
@Table(name = "cardsets")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cardset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private int complexity;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    @OneToMany(mappedBy = "cardset", cascade = CascadeType.ALL)
    private Set<Card> cards;

    @Setter(AccessLevel.NONE)
    private int progress;

    @OneToMany(mappedBy = "cardset", cascade = CascadeType.ALL)
    private Set<CardsetLearnStat> learnStats;

    public Cardset(String cardsetName, User cardsetOwner) {
        this.name = cardsetName;
        this.complexity = INIT_COMPLEXITY;
        this.owner = cardsetOwner;
        this.cards = new HashSet<>();
    }
}
