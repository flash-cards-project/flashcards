package com.netcracker.flashcards.entity;

import com.netcracker.flashcards.dto.CardsetCardQADTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static com.netcracker.flashcards.util.constants.CardConstants.MIN_TIER;

@Entity
@Table(name = "cards")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String question;
    private String answer;
    private int tier;

    @Column(name = "steps_left")
    private int stepsLeft;

    @ManyToOne
    @JoinColumn(name = "set_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Cardset cardset;

    public Card(CardsetCardQADTO cardsetCardQADTO, Cardset cardset) {
        this.question = cardsetCardQADTO.getQuestion();
        this.answer = cardsetCardQADTO.getAnswer();
        this.tier = MIN_TIER;
        this.stepsLeft = cardset.getComplexity();
        this.cardset = cardset;
    }
}
