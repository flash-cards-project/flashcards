package com.netcracker.flashcards.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.ZonedDateTime;

@Entity
@Table(name = "cardset_learn_stats")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CardsetLearnStat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "set_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Cardset cardset;

    private ZonedDateTime time;
    private int progressPoint;
}
