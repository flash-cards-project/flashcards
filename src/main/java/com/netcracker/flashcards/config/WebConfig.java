package com.netcracker.flashcards.config;

import com.netcracker.flashcards.util.converter.StringToIntegerConverter;
import com.netcracker.flashcards.util.converter.StringToLevelOfKnowledgeEnumConverter;
import com.netcracker.flashcards.util.converter.StringToLongConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToLevelOfKnowledgeEnumConverter());
        registry.addConverter(new StringToLongConverter());
        registry.addConverter(new StringToIntegerConverter());
    }
}
