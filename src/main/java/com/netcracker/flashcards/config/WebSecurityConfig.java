package com.netcracker.flashcards.config;

import com.netcracker.flashcards.entity.Card;
import com.netcracker.flashcards.service.CardService;
import com.netcracker.flashcards.service.CardsetService;
import com.netcracker.flashcards.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Autowired
    private CardsetService cardsetService;

    @Autowired
    private CardService cardService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                    //react spa
                    .antMatchers("/", "/public/**", "/index.bundle.js", "/main.css").permitAll()
                    .antMatchers("/api/login").permitAll()
                    .antMatchers("/api/register").permitAll()
                    .antMatchers("/api/cardsets/search/**").fullyAuthenticated()
                    .antMatchers("/api/cardsets/{id:\\d+}/**")
                        .access("isFullyAuthenticated() and @webSecurityConfig.checkCardsetOwner(authentication, #id)")
                    .antMatchers("/api/cardsets/**").fullyAuthenticated()
                    .antMatchers("/api/cards/search/**").fullyAuthenticated()
                    .antMatchers("/api/cards/{id:\\d+}/**")
                        .access("isFullyAuthenticated() and @webSecurityConfig.checkCardOwner(authentication, #id)")
                    .antMatchers("/api/cards/**").fullyAuthenticated()
                    .anyRequest().authenticated()
                .and().httpBasic()
                    .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
                .and().logout()
                    .logoutUrl("/api/logout")
                    .logoutSuccessUrl("/")
                    .deleteCookies("JSESSIONID")
                    .invalidateHttpSession(true);
    }

    public boolean checkCardsetOwner(Authentication authentication, long id) {
        return cardsetService.isCardsetExist(id)
                && cardsetService.isUserOwnCardset(id, authentication.getName());
    }

    public boolean checkCardOwner(Authentication authentication, long cardId) {
        Card card = cardService.getCardById(cardId);
        return checkCardsetOwner(authentication, card.getCardset().getId());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("http://localhost:3000"));
        configuration.setAllowedMethods(List.of("*"));
        configuration.setAllowedHeaders(Arrays.asList(
                "X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization"));
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
