package com.netcracker.flashcards.exception;

public class CardsetNotFoundException extends RuntimeException {
    public CardsetNotFoundException(String message) {
        super(message);
    }
}
