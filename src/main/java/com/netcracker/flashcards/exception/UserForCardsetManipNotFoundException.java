package com.netcracker.flashcards.exception;

public class UserForCardsetManipNotFoundException extends RuntimeException {
    public UserForCardsetManipNotFoundException(String message) {
        super(message);
    }
}
