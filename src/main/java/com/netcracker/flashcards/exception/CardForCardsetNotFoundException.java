package com.netcracker.flashcards.exception;

public class CardForCardsetNotFoundException extends RuntimeException {
    public CardForCardsetNotFoundException(String message) {
        super(message);
    }
}
