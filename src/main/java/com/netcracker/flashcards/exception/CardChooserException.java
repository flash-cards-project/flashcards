package com.netcracker.flashcards.exception;

public class CardChooserException extends RuntimeException {
    public CardChooserException() {
        super();
    }

    public CardChooserException(String message) {
        super(message);
    }
}
