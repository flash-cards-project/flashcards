package com.netcracker.flashcards.exception;

public class UserLearnSessionException extends RuntimeException {
    public UserLearnSessionException() {
        super();
    }

    public UserLearnSessionException(String message) {
        super(message);
    }

    public UserLearnSessionException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserLearnSessionException(Throwable cause) {
        super(cause);
    }

    protected UserLearnSessionException(String message, Throwable cause,
                                        boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
