package com.netcracker.flashcards.exception;

public class CardsetAlreadyHasCardWithQuestionException extends RuntimeException {
    public CardsetAlreadyHasCardWithQuestionException (String message) {
        super(message);
    }
}
