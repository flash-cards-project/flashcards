package com.netcracker.flashcards.exception;

public class UserAlreadyHasCardsetException extends RuntimeException {
    public UserAlreadyHasCardsetException(String message) {
        super(message);
    }
}
