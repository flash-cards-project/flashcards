package com.netcracker.flashcards.repository;

import com.netcracker.flashcards.entity.CardsetLearnStat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CardsetLearnStatRepository extends JpaRepository<CardsetLearnStat, Long> {
    List<CardsetLearnStat> findAllByCardsetId(long cardsetId);
}
