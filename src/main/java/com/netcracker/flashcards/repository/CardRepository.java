package com.netcracker.flashcards.repository;

import com.netcracker.flashcards.entity.Card;
import com.netcracker.flashcards.entity.ProjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.netcracker.flashcards.entity.Cardset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;


@Repository
public interface CardRepository extends JpaRepository<Card, Long> {
    Optional<Card> findCardByCardsetAndId(Cardset cardset, long id);

    Page<Card> findAllByCardsetId(long cardsetId, Pageable pageable);

    long countAllByCardsetId(long cardsetId);

    long countAllByCardsetIdAndTier(long cardsetId, int tier);

    Set<ProjectId> findAllIdsByCardsetIdAndTier(long cardsetId, int tier);

    @Query(value = "SELECT * FROM cards WHERE set_id = ?1 ORDER BY random() LIMIT 1",
            nativeQuery = true)
    Card getRandomCardByCardsetId(long cardsetId);

    @Query(value = "SELECT id FROM cards WHERE set_id = ?1 AND tier = ?2 ORDER BY random() LIMIT ?3",
            nativeQuery = true)
    Set<ProjectId> getNRandomCardsIdsByCardsetIdAndTier(long cardsetId, int tier, int n);

    @Query(value = "SELECT * FROM cards WHERE (set_id = ?1 AND (question LIKE concat('%', ?2, " +
            "'%') OR answer LIKE concat('%', ?2, '%')) AND (?3 = 0 OR tier = ?3))", nativeQuery = true)
    Set<Card> findAllByCardsetIdAndQueryAndLevel(long cardsetId, String query, int level);
}
