package com.netcracker.flashcards.repository;

import com.netcracker.flashcards.entity.CardsetLearnSession;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface CardsetLearnSessionRepository extends JpaRepository<CardsetLearnSession, Long> {
    Set<CardsetLearnSession> findAllByCardsetId(long cardsetId);
}
