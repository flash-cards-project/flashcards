package com.netcracker.flashcards.repository;

import com.netcracker.flashcards.entity.Cardset;
import com.netcracker.flashcards.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.Optional;

@Repository
public interface CardsetRepository extends JpaRepository<Cardset, Long> {
    Optional<Cardset> findCardsetByIdAndOwner(long id, User owner);

    long countAllById(long id);

    long countAllByIdAndOwner(long id, User owner);

    Set<Cardset> findAllByOwner(User user);

    @Query(value = "SELECT * FROM cardsets WHERE (owner_id = ?1 AND name LIKE concat('%', ?2, " +
            "'%'))", nativeQuery = true)
    Set<Cardset> findAllByOwnerIdAndQuery(long ownerId, String query);
}
