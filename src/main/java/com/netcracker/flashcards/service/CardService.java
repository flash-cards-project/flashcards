package com.netcracker.flashcards.service;

import com.netcracker.flashcards.dto.CardDTO;
import com.netcracker.flashcards.dto.CardsSearchDTO;
import com.netcracker.flashcards.dto.CardsetCardQADTO;
import com.netcracker.flashcards.dto.CardsetIdDTO;
import com.netcracker.flashcards.entity.Card;
import com.netcracker.flashcards.entity.Cardset;
import com.netcracker.flashcards.exception.CardForCardsetNotFoundException;
import com.netcracker.flashcards.exception.CardNotFoundException;
import com.netcracker.flashcards.exception.CardsetAlreadyHasCardWithQuestionException;
import com.netcracker.flashcards.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CardService {

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private CardsetService cardsetService;

    private Set<String> getCardsetCardsQuestions(Cardset cardset) {
        return cardset.getCards().stream()
                .map(Card::getQuestion)
                .collect(Collectors.toSet());
    }

    private void normalizeQA(CardsetCardQADTO cardsetCardQADTO) {
        cardsetCardQADTO.setQuestion(cardsetCardQADTO.getQuestion().trim());
        cardsetCardQADTO.setAnswer(cardsetCardQADTO.getAnswer().trim());
    }

    private void checkQuestionInCardset(String question, Cardset cardset) {
        if (getCardsetCardsQuestions(cardset).contains(question)) {
            throw new CardsetAlreadyHasCardWithQuestionException("Cardset already has the card " +
                    "with this question.");
        }
    }

    public Card getCardByIdAndCardsetId(long cardId, long cardsetId) {
        Card card = getCardById(cardId);
        if (card.getCardset().getId() != cardsetId) {
            throw new CardForCardsetNotFoundException(String.format("Card with " +
                    "id %s for cardset with id %s not found.", cardsetId, cardId));
        }
        return card;
    }

    public void resetCardStepsLeft(Card card) {
        card.setStepsLeft(card.getCardset().getComplexity());
        cardRepository.save(card);
    }

    public Set<CardDTO> getListOfCards(CardsetIdDTO cardsetIdDTO) {
        Cardset cardset = cardsetService.getCardsetById(cardsetIdDTO.getCardsetId());
        return cardset.getCards().stream()
                .map(CardDTO::new)
                .collect(Collectors.toSet());
    }

    public Card getCardById(long cardId) {
        return cardRepository.findById(cardId)
                .orElseThrow(() -> {
                    String errorMessage = String.format("Card with id %d not found.", cardId);
                    return new CardNotFoundException(errorMessage);
                });
    }

    public CardDTO addNewCard(CardsetCardQADTO cardsetCardQADTO) {
        Cardset cardset = cardsetService.getCardsetById(cardsetCardQADTO.getCardsetId());
        normalizeQA(cardsetCardQADTO);
        checkQuestionInCardset(cardsetCardQADTO.getQuestion(), cardset);
        Card card = new Card(cardsetCardQADTO, cardset);
        cardRepository.save(card);
        return new CardDTO(card);
    }

    public void deleteCardsetCard(CardsetIdDTO cardsetIdDTO, long cardId) {
        Card card = getCardByIdAndCardsetId(cardId, cardsetIdDTO.getCardsetId());
        cardRepository.delete(card);
    }

    public CardDTO renameCard(CardsetCardQADTO cardsetCardQADTO, long cardId) {
        Card card = getCardByIdAndCardsetId(cardId, cardsetCardQADTO.getCardsetId());
        Cardset cardset = cardsetService.getCardsetById(cardsetCardQADTO.getCardsetId());
        normalizeQA(cardsetCardQADTO);
        if (!cardsetCardQADTO.getQuestion().equals(card.getQuestion())) {
            checkQuestionInCardset(cardsetCardQADTO.getQuestion(), cardset);
            card.setQuestion(cardsetCardQADTO.getQuestion());
        }
        card.setAnswer(cardsetCardQADTO.getAnswer());
        cardRepository.save(card);
        return new CardDTO(card);
    }

    public Set<CardDTO> getListOfCardsByQuery(CardsSearchDTO cardsSearchDTO) {
        if (cardsSearchDTO.getLevel() == 0 && cardsSearchDTO.getQuery().length() == 0) {
            return getListOfCards(new CardsetIdDTO(cardsSearchDTO.getCardsetId()));
        }
        Set<Card> cards = cardRepository.findAllByCardsetIdAndQueryAndLevel(
                cardsSearchDTO.getCardsetId(), cardsSearchDTO.getQuery(), cardsSearchDTO.getLevel());
        return cards.stream().map(CardDTO::new).collect(Collectors.toSet());
    }
}
