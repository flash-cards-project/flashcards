package com.netcracker.flashcards.service.chooser;

import com.netcracker.flashcards.dto.CardDTO;
import com.netcracker.flashcards.entity.Card;
import com.netcracker.flashcards.entity.ProjectId;
import com.netcracker.flashcards.exception.CardChooserException;
import com.netcracker.flashcards.repository.CardRepository;
import com.netcracker.flashcards.util.Round;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.netcracker.flashcards.util.constants.CardConstants.MAX_TIER;
import static com.netcracker.flashcards.util.constants.CardConstants.MIN_TIER;

@Service
@Slf4j
public class Round15CardChooser extends CardChooser {
    private static final int roundSize = 15;

    @Autowired
    private CardRepository cardRepository;

    private static int getReqNumOfCardsByTier(int tier) {
        // require 5 cards from 1st tier, 4 from 2nd... 1 from 5th
        return (MAX_TIER + 1) - tier;
    }

    @Override
    public CardDTO getNextCard(long cardsetId, @NonNull Round round) {
        if (round.size() == 0) {
            log.info("Round(cardsetId={}) is empty", cardsetId);
            generateNewRound(cardsetId, round);
        }
        log.debug("Pop Card(id={}) from Round(cardsetId={})", round.peek(), cardsetId);
        Card card = cardRepository.findById(round.pop()).orElseThrow(CardChooserException::new);
        return new CardDTO(card);
    }

    private void generateNewRound(long cardsetId, @NonNull Round round) {
        round.clear();

        // count number of cards by tier
        Map<Integer, Long> numOfCardsByTier = new HashMap<>();
        forEachTier((tier) -> {
            long numOfCards = cardRepository.countAllByCardsetIdAndTier(cardsetId, tier);
            numOfCardsByTier.put(tier, numOfCards);
        });
        log.debug("Cardset(id={}) number of cards by tier: {}", cardsetId, numOfCardsByTier);

        long cardsShortage = numOfCardsByTier.entrySet().stream()
                .reduce(0L, (shortage, entry) -> {
                        int tier = entry.getKey();
                        long numOfCards = entry.getValue();
                        return shortage + Math.max(0, getReqNumOfCardsByTier(tier) - numOfCards);
                    }, Long::sum);
        log.debug("Cardset(id={}) cards shortage: {}", cardsetId, cardsShortage);

        var wrapper = new Object() { long shortage = cardsShortage; };
        forEachTier((tier) -> {
            int reqNumOfCards = getReqNumOfCardsByTier(tier);
            long totalNumOfCards = numOfCardsByTier.get(tier);

            long cardsDraw = 0;
            if (totalNumOfCards > reqNumOfCards && wrapper.shortage > 0) {
                cardsDraw = Math.min(totalNumOfCards - reqNumOfCards, wrapper.shortage);
                wrapper.shortage -= cardsDraw;
                reqNumOfCards += cardsDraw;
            }

            //debug log
            if (log.isDebugEnabled() && cardsDraw > 0) {
                log.debug("Round(cardsetId={}) draw {} cards from tier {}", cardsetId, cardsDraw, tier);
            }

            Set<ProjectId> cardIds;
            if (totalNumOfCards <= reqNumOfCards) {
                cardIds = cardRepository.findAllIdsByCardsetIdAndTier(cardsetId, tier);
            } else {
                cardIds = cardRepository.getNRandomCardsIdsByCardsetIdAndTier(cardsetId, tier, reqNumOfCards);
            }
            round.addAll(cardIds.stream().map(ProjectId::getId).collect(Collectors.toSet()));
        });
        log.info("Generated new Round(cardsetId={})", cardsetId);

        // debug log
        if (log.isDebugEnabled()) {
            StringBuilder tiers = new StringBuilder(roundSize);
            StringBuilder ids = new StringBuilder();
            for (long id : round) {
                Card card = cardRepository.findById(id).orElseThrow(CardChooserException::new);
                tiers.append(card.getTier());
                ids.append(card.getId()).append(" ");
            }
            log.debug("Round(cardsetId={}) tiers: {}", cardsetId, tiers);
            log.debug("Round(cardsetId={}) ids: {}", cardsetId, ids);
        }

        round.shuffle();
    }
}
