package com.netcracker.flashcards.service.chooser;

import com.netcracker.flashcards.dto.CardDTO;
import com.netcracker.flashcards.util.Round;

import java.util.function.Consumer;

import static com.netcracker.flashcards.util.constants.CardConstants.MAX_TIER;
import static com.netcracker.flashcards.util.constants.CardConstants.MIN_TIER;

public abstract class CardChooser {
    public abstract CardDTO getNextCard(long cardsetId, Round round);

    void forEachTier(Consumer<Integer> consumer) {
        for (int tier = MIN_TIER; tier < MAX_TIER + 1; tier++) {
            consumer.accept(tier);
        }
    }
}
