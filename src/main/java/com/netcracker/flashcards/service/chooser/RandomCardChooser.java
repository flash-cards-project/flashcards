package com.netcracker.flashcards.service.chooser;

import com.netcracker.flashcards.dto.CardDTO;
import com.netcracker.flashcards.repository.CardRepository;
import com.netcracker.flashcards.util.Round;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RandomCardChooser extends CardChooser {
    @Autowired
    private CardRepository cardRepository;

    @Override
    public CardDTO getNextCard(long cardsetId, Round round) {
        return new CardDTO(cardRepository.getRandomCardByCardsetId(cardsetId));
    }
}
