package com.netcracker.flashcards.service;

import com.netcracker.flashcards.dto.CardsetLearnSessionDTO;
import com.netcracker.flashcards.dto.CardsetLearnSessionListDTO;
import com.netcracker.flashcards.entity.CardsetLearnSession;
import com.netcracker.flashcards.repository.CardsetLearnSessionRepository;
import com.netcracker.flashcards.util.TimeUtil;
import com.netcracker.flashcards.util.UserLearnSession;
import com.netcracker.flashcards.util.constants.CardConstants;
import com.netcracker.flashcards.util.constants.CardsetConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CardsetLearnSessionService {

    @Autowired
    private CardsetLearnSessionRepository cardsetLearnSessionRepository;

    @Autowired
    private CardsetService cardsetService;

    private final Map<Long, UserLearnSession> learnSessions = new ConcurrentHashMap<>();

    @Scheduled(fixedDelay = CardsetConstants.SESSION_SAVER_DELAY)
    private void stopExpiredLearnSessionsTask() {
        log.info("stopExpiredLearnSessionsTask() started");

        for (Map.Entry<Long, UserLearnSession> entry : learnSessions.entrySet()) {
            if (isSessionExpired(entry.getKey())) {
                stopLearnSession(entry.getKey());
            }
        }
    }

    public void startLearnSession(long cardsetId) {
        learnSessions.put(cardsetId, new UserLearnSession());

        log.info("Create learn session [cardsetId={}]", cardsetId);
    }

    public void stopLearnSession(long cardsetId) {
        if (!isSessionExisted(cardsetId)) {
            return;
        }

        UserLearnSession learnSession = learnSessions.get(cardsetId);
        CardsetLearnSession cardsetLearnSession = CardsetLearnSession.builder()
                .startTime(learnSession.getStartTime())
                .stopTime(learnSession.getLastLogTime())
                .goodCount(learnSession.getCountOfLevel(CardConstants.LEVEL_OF_KNOWLEDGE.GOOD))
                .badCount(learnSession.getCountOfLevel(CardConstants.LEVEL_OF_KNOWLEDGE.BAD))
                .cardset(cardsetService.getCardsetById(cardsetId))
                .build();
        cardsetLearnSessionRepository.save(cardsetLearnSession);
        learnSessions.remove(cardsetId);

        log.info("Learn session saved to db [cardsetId={}]: {}", cardsetId, cardsetLearnSession);
    }

    public void logLevel(long cardsetId, CardConstants.LEVEL_OF_KNOWLEDGE levelOfKnowledge) {
        if (levelOfKnowledge == CardConstants.LEVEL_OF_KNOWLEDGE.UNDEFINED) {
            return;
        }

        if (!isSessionExisted(cardsetId)) {
            startLearnSession(cardsetId);
        }
        UserLearnSession learnSession = learnSessions.get(cardsetId);
        learnSession.logLevel(levelOfKnowledge);
    }

    public boolean isSessionExisted(long cardsetId) {
        return learnSessions.get(cardsetId) != null;
    }

    private boolean isSessionExpired(long cardsetId) {
        return Duration.between(learnSessions.get(cardsetId).getLastLogTime(),
                TimeUtil.nowUtcZoned()).toMillis() >= CardsetConstants.LEARN_SESSION_TIMEOUT;
    }

    public CardsetLearnSessionListDTO getLearnSessionList(long cardsetId) {
        Set<CardsetLearnSession> sessions = cardsetLearnSessionRepository.findAllByCardsetId(cardsetId);
        return new CardsetLearnSessionListDTO(cardsetId,
                sessions.stream().map(CardsetLearnSessionDTO::new).collect(Collectors.toSet()));
    }
}
