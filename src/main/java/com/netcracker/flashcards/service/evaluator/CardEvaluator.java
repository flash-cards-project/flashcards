package com.netcracker.flashcards.service.evaluator;

import com.netcracker.flashcards.entity.Card;
import com.netcracker.flashcards.util.constants.CardConstants.LEVEL_OF_KNOWLEDGE;

public interface CardEvaluator {
    void evaluate(Card card, LEVEL_OF_KNOWLEDGE levelOfKnowledge);
}
