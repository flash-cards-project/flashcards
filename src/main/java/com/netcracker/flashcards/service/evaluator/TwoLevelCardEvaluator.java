package com.netcracker.flashcards.service.evaluator;

import com.netcracker.flashcards.entity.Card;
import com.netcracker.flashcards.entity.Cardset;
import com.netcracker.flashcards.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.netcracker.flashcards.util.constants.CardConstants.MAX_TIER;
import static com.netcracker.flashcards.util.constants.CardConstants.MIN_TIER;
import static com.netcracker.flashcards.util.constants.CardsetConstants.MIN_COMPLEXITY;
import com.netcracker.flashcards.util.constants.CardConstants.LEVEL_OF_KNOWLEDGE;

@Service
public class TwoLevelCardEvaluator implements CardEvaluator {

    @Autowired
    CardRepository cardRepository;

    @Override
    public void evaluate(Card card, LEVEL_OF_KNOWLEDGE levelOfKnowledge) {
        Cardset cardset = card.getCardset();
        if (levelOfKnowledge.equals(LEVEL_OF_KNOWLEDGE.BAD)) {
            card.setStepsLeft(card.getStepsLeft() + 1);
            if (card.getStepsLeft() > cardset.getComplexity()) {
                if (card.getTier() != MIN_TIER) {
                    card.setTier(card.getTier() - 1);
                    card.setStepsLeft(MIN_COMPLEXITY);
                } else {
                    card.setStepsLeft(cardset.getComplexity());
                }
            }
        } else if (levelOfKnowledge.equals(LEVEL_OF_KNOWLEDGE.GOOD)) {
            card.setStepsLeft(card.getStepsLeft() - 1);
            if (card.getStepsLeft() == 0) {
                if (card.getTier() != MAX_TIER) {
                    card.setTier(card.getTier() + 1);
                    card.setStepsLeft(cardset.getComplexity());
                } else {
                    card.setStepsLeft(MIN_COMPLEXITY);
                }
            }
        }
        cardRepository.save(card);
    }
}
