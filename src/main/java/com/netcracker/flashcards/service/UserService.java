package com.netcracker.flashcards.service;

import com.netcracker.flashcards.dto.SignUpDTO;
import com.netcracker.flashcards.entity.User;
import com.netcracker.flashcards.exception.UserAlreadyRegistered;
import com.netcracker.flashcards.exception.UserForCardsetManipNotFoundException;
import com.netcracker.flashcards.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void signUp(SignUpDTO signUpDTO) {
        Optional<User> foundUser = userRepository.findByUsername(signUpDTO.getUsername());
        if (foundUser.isPresent()) {
            throw new UserAlreadyRegistered(String.format(
                    "Username %s is already occupied.", signUpDTO.getUsername()));
        }
        User user = new User(signUpDTO, bCryptPasswordEncoder.encode(signUpDTO.getPassword()));
        userRepository.save(user);
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() ->
                new UserForCardsetManipNotFoundException(String.format(
                        "User with username %s not found.", username)));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException(String.format(
                        "User with username %s doesn't exist.", username)));

        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(), new HashSet<>());
    }
}
