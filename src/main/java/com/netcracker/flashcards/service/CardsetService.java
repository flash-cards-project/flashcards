package com.netcracker.flashcards.service;

import com.netcracker.flashcards.dto.CardDTO;
import com.netcracker.flashcards.dto.CardsetComplexityDTO;
import com.netcracker.flashcards.dto.CardsetDTO;
import com.netcracker.flashcards.dto.CardsetLearnStatsDTO;
import com.netcracker.flashcards.dto.CardsetNameDTO;
import com.netcracker.flashcards.dto.CardLevelOfKnowledgeDTO;
import com.netcracker.flashcards.dto.CardsetsSearchDTO;
import com.netcracker.flashcards.entity.Card;
import com.netcracker.flashcards.entity.Cardset;
import com.netcracker.flashcards.entity.CardsetLearnStat;
import com.netcracker.flashcards.entity.User;
import com.netcracker.flashcards.exception.CardsetNotFoundException;
import com.netcracker.flashcards.exception.UserAlreadyHasCardsetException;
import com.netcracker.flashcards.repository.CardsetLearnStatRepository;
import com.netcracker.flashcards.repository.CardsetRepository;
import com.netcracker.flashcards.service.evaluator.CardEvaluator;
import com.netcracker.flashcards.service.chooser.CardChooser;
import com.netcracker.flashcards.util.Round;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.netcracker.flashcards.util.constants.CardConstants.LEVEL_OF_KNOWLEDGE.UNDEFINED;

@Service
public class CardsetService {

    @Autowired
    private CardService cardService;

    @Autowired
    private CardsetRepository cardsetRepository;

    @Autowired
    private CardsetLearnStatRepository cardsetLearnStatRepository;

    @Autowired
    private UserService userService;

    @Qualifier("round15CardChooser")
    @Autowired
    private CardChooser cardChooser;

    @Autowired
    private CardEvaluator cardEvaluator;

    private Set<String> getUserCardsetsNames(User user) {
        return cardsetRepository.findAllByOwner(user).stream()
                .map(Cardset::getName)
                .collect(Collectors.toSet());
    }

    private String normalizeCardsetName(String cardsetName) {
        cardsetName = cardsetName.replaceAll("(\\r\\n|\\n|\\r)", "");
        return cardsetName.replaceAll("(\\s\\s+)", " ");
    }

    private void validateCardsetName(String newCardsetName, User user) {
        Set<String> userCardsetsNames = getUserCardsetsNames(user);
        if (userCardsetsNames.contains(newCardsetName)) {
            throw new UserAlreadyHasCardsetException(String.format("You already has cardset " +
                    "with name %s.", newCardsetName));
        }
    }

    private String generateCardsetName(Set<String> userCardsetsNames) {
        String newCardsetName;
        for (long i = 1; ; ++i) {
            newCardsetName = "Cardset #" + i;
            if (!userCardsetsNames.contains(newCardsetName))
                return newCardsetName;
        }
    }

    public Cardset getCardsetById(long cardsetId) {
        return cardsetRepository.findById(cardsetId)
                .orElseThrow(() -> {
                    String errorMessage = String.format("Cardset with %d id not found", cardsetId);
                    return new CardsetNotFoundException(errorMessage);
                });
    }

    public boolean isCardsetExist(long cardsetId) {
        return cardsetRepository.countAllById(cardsetId) == 1;
    }

    public boolean isUserOwnCardset(long cardsetId, String username) {
        User user = userService.getUserByUsername(username);
        return cardsetRepository.countAllByIdAndOwner(cardsetId, user) == 1;
    }

    public Set<CardsetDTO> getListOfCardsets(Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        return user.getCardsets().stream()
                .map(CardsetDTO::new)
                .collect(Collectors.toSet());
    }

    public CardsetDTO addNewCardset(Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        Set<String> userCardsetsNames = getUserCardsetsNames(user);
        String newCardsetName = generateCardsetName(userCardsetsNames);
        Cardset cardset = new Cardset(newCardsetName, user);
        cardsetRepository.save(cardset);
        return new CardsetDTO(cardset);
    }

    public void deleteCardset(long cardsetId) {
        Cardset cardset = getCardsetById(cardsetId);
        cardsetRepository.delete(cardset);
    }

    public CardsetDTO renameCardset(
            long cardsetId, CardsetNameDTO cardsetNameDTO, Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        Cardset cardset = getCardsetById(cardsetId);
        String newCardsetName = normalizeCardsetName(cardsetNameDTO.getName());
        if (!newCardsetName.equals(cardset.getName())) {
            validateCardsetName(newCardsetName, user);
            cardset.setName(newCardsetName);
            cardsetRepository.save(cardset);
        }
        return new CardsetDTO(cardset);
    }

    public CardsetDTO changeCardsetComplexity(
            long cardsetId, CardsetComplexityDTO cardsetComplexityDTO) {
        Cardset cardset = getCardsetById(cardsetId);
        if (cardset.getComplexity() != cardsetComplexityDTO.getComplexity()) {
            cardset.setComplexity(cardsetComplexityDTO.getComplexity());
            cardsetRepository.save(cardset);
            cardset.getCards().forEach(cardService::resetCardStepsLeft);
        }
        return new CardsetDTO(cardset);
    }

    public CardDTO getNextCard(long cardsetId, CardLevelOfKnowledgeDTO levelOfKnowledgeOfCardDTO, Round round) {
        /* The level indicates the level of knowledge of the previous issued card.
           Undefined level means getting 1st card (without previous card). */
        if (levelOfKnowledgeOfCardDTO.getLevel() != UNDEFINED) {
            Card card = cardService.getCardByIdAndCardsetId(
                    levelOfKnowledgeOfCardDTO.getCardId(), cardsetId);
            cardEvaluator.evaluate(card, levelOfKnowledgeOfCardDTO.getLevel());
        }
        return cardChooser.getNextCard(cardsetId, round);
    }

    public Set<CardsetDTO> getListOfCardsetsByQuery(
            CardsetsSearchDTO cardsetsSearchDTO, Principal principal) {
        if (cardsetsSearchDTO.getQuery().length() == 0) {
            return getListOfCardsets(principal);
        }
        User user = userService.getUserByUsername(principal.getName());
        Set<Cardset> cardsets = cardsetRepository.findAllByOwnerIdAndQuery(
                user.getId(), cardsetsSearchDTO.getQuery());
        return cardsets.stream().map(CardsetDTO::new).collect(Collectors.toSet());
    }

    public CardsetLearnStatsDTO getLearnStats(long cardsetId) {
        List<CardsetLearnStat> stats = cardsetLearnStatRepository.findAllByCardsetId(cardsetId);
        return new CardsetLearnStatsDTO(stats);
    }
}
