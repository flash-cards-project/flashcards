-- create tables
CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(32) UNIQUE NOT NULL,
    password VARCHAR(60) NOT NULL
);

CREATE TABLE cardsets (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(32) NOT NULL,
    complexity INTEGER NOT NULL,
    owner_id BIGINT NOT NULL REFERENCES users (id),
    progress INTEGER NOT NULL DEFAULT 0
      CHECK (progress BETWEEN 0 AND 100)
);

CREATE TABLE cards (
    id BIGSERIAL PRIMARY KEY,
    question VARCHAR(256) NOT NULL,
    tier INTEGER NOT NULL,
    steps_left INTEGER NOT NULL,
    answer VARCHAR(256) NOT NULL,
    set_id BIGINT NOT NULL REFERENCES cardsets (id)
        ON DELETE CASCADE
);

CREATE TABLE cardset_learn_stats (
    id BIGSERIAL PRIMARY KEY,
    set_id BIGINT NOT NULL REFERENCES cardsets (id)
        ON DELETE CASCADE,
    time TIMESTAMPTZ NOT NULL,
    progress_point INTEGER NOT NULL
        CHECK (progress_point BETWEEN 0 AND 100)
);

CREATE TABLE cardset_learn_sessions (
    id BIGSERIAL PRIMARY KEY,
    set_id BIGINT NOT NULL REFERENCES cardsets (id)
        ON DELETE CASCADE,
    start_time TIMESTAMPTZ NOT NULL,
    stop_time TIMESTAMPTZ NOT NULL,
    good_count INTEGER NOT NULL
        CHECK (good_count >= 0),
    bad_count INTEGER NOT NULL
        CHECK (bad_count >= 0)
);

-- create functions
CREATE OR REPLACE FUNCTION get_cur_cardset_progress(_id BIGINT) RETURNS INTEGER AS $$
DECLARE
    max_sum BIGINT;
    cur_sum BIGINT;
BEGIN
    SELECT COUNT(*) * 4, SUM(tier - 1) INTO max_sum, cur_sum
    FROM cards WHERE set_id = _id;
    max_sum := GREATEST(max_sum, 1);
    cur_sum := COALESCE(cur_sum, 0);
    RETURN ROUND(100 * cur_sum / max_sum);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_last_progress_point(_id BIGINT) RETURNS INTEGER AS $$
SELECT progress_point FROM cardset_learn_stats
WHERE set_id = _id
ORDER BY time DESC LIMIT 1;
$$ LANGUAGE SQL;

-- create trigger procedures
CREATE OR REPLACE FUNCTION calc_cardset_progress() RETURNS TRIGGER AS $$
DECLARE
    cur_progress INTEGER;
    _id BIGINT;
BEGIN
    _id := COALESCE(NEW.set_id, OLD.set_id);
    cur_progress := get_cur_cardset_progress(_id);
    UPDATE cardsets SET progress = cur_progress WHERE id = _id;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_cardset_progress() RETURNS TRIGGER AS $$
DECLARE
    min_progress_diff CONSTANT INTEGER := 5;
    cur_progress INTEGER;
    old_progress INTEGER;
    _id BIGINT;
BEGIN
    _id := COALESCE(NEW.set_id, OLD.set_id);
    SELECT progress INTO cur_progress FROM cardsets
    WHERE id = _id;
    old_progress := get_last_progress_point(_id);
    IF (old_progress IS NULL OR
        ABS(old_progress - cur_progress) >= min_progress_diff) THEN
        INSERT INTO cardset_learn_stats (set_id, time, progress_point)
        VALUES (_id, now(), cur_progress);
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- create triggers
CREATE TRIGGER a_calc_cardset_progress
AFTER INSERT OR UPDATE OR DELETE on cards
    FOR EACH ROW EXECUTE PROCEDURE calc_cardset_progress();

CREATE TRIGGER b_log_cardset_progress
AFTER INSERT OR UPDATE OR DELETE on cards
    FOR EACH ROW EXECUTE PROCEDURE log_cardset_progress();