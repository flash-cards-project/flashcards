package com.netcracker.flashcards.util.constants;

import static com.netcracker.flashcards.util.constants.AuthConstants.MAX_USERNAME_LENGTH;
import static com.netcracker.flashcards.util.constants.AuthConstants.MAX_PASSWORD_LENGTH;
import static com.netcracker.flashcards.util.constants.AuthConstants.MIN_PASSWORD_LENGTH;
import static com.netcracker.flashcards.util.constants.AuthConstants.MIN_USERNAME_LENGTH;

public class AuthTestConstants {
    // Incorrect credentials for testing
    public static final String TOO_LONG_USERNAME = "x".repeat(MAX_USERNAME_LENGTH + 1);
    public static final String TOO_LONG_PASSWORD = "x".repeat(MAX_PASSWORD_LENGTH + 1);
    public static final String TOO_SHORT_CREDENTIAL = "x";
    public static final String NOT_PATTERNED_USERNAME = "x".repeat(MIN_USERNAME_LENGTH) + '#';
    public static final String NOT_PATTERNED_PASSWORD = "x".repeat(MIN_PASSWORD_LENGTH) + '.';

    // Correct credentials for testing
    public static final String CORRECT_USERNAME = "x".repeat(MIN_USERNAME_LENGTH + 1);
    public static final String CORRECT_PASSWORD = "x".repeat(MIN_PASSWORD_LENGTH + 1);

    // Test credentials
    public static final String TEST_USERNAME = "test";
    public static final String TEST_PASSWORD = "Weak_p4ss";
}
