package com.netcracker.flashcards.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.flashcards.dto.SignUpDTO;
import com.netcracker.flashcards.entity.User;
import com.netcracker.flashcards.repository.UserRepository;
import com.netcracker.flashcards.service.UserService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.netcracker.flashcards.util.constants.AuthTestConstants.CORRECT_PASSWORD;
import static com.netcracker.flashcards.util.constants.AuthTestConstants.CORRECT_USERNAME;
import static com.netcracker.flashcards.util.constants.AuthTestConstants.NOT_PATTERNED_PASSWORD;
import static com.netcracker.flashcards.util.constants.AuthTestConstants.NOT_PATTERNED_USERNAME;
import static com.netcracker.flashcards.util.constants.AuthTestConstants.TEST_PASSWORD;
import static com.netcracker.flashcards.util.constants.AuthTestConstants.TEST_USERNAME;
import static com.netcracker.flashcards.util.constants.AuthTestConstants.TOO_LONG_PASSWORD;
import static com.netcracker.flashcards.util.constants.AuthTestConstants.TOO_LONG_USERNAME;
import static com.netcracker.flashcards.util.constants.AuthTestConstants.TOO_SHORT_CREDENTIAL;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;


    private void requestRegistrationVerification(
            SignUpDTO signUpDTO, ResultMatcher status) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/register")
                        .content(mapper.writeValueAsString(signUpDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status);
    }

    private void requestLoginVerification(
            String username, String password, ResultMatcher status) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/login").with(httpBasic(username, password)))
                .andExpect(status);
    }

    private void deleteUser(String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException("Stab exception!"));
        userRepository.delete(user);
    }

    @BeforeAll
    public void setUp() {
        userService.signUp(new SignUpDTO(TEST_USERNAME, TEST_PASSWORD));
    }

    @AfterAll
    public void cleanUp() {
        // Will be created in correct registration test
        deleteUser(CORRECT_USERNAME);
        deleteUser(TEST_USERNAME);
    }

    // Login tests
    @Test
    public void getUnauthorizedAccessToLoginPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/login"))
                .andExpect(status().isOk());
    }

    @Test
    public void loginToExistingAccount() throws Exception {
        requestLoginVerification(TEST_USERNAME, TEST_PASSWORD, status().isOk());
    }

    @Test
    public void loginToDoesNotExistingAccount() throws Exception {
        requestLoginVerification(TOO_LONG_USERNAME, TOO_LONG_PASSWORD, status().isUnauthorized());
    }

    // Registration tests
    @Test
    public void registerUserWithTooLongUsername() throws Exception {
        SignUpDTO invalidSignUpDTO = new SignUpDTO(TOO_LONG_USERNAME, CORRECT_PASSWORD);
        requestRegistrationVerification(invalidSignUpDTO, status().isConflict());
    }

    @Test
    public void registerUserWithShortUsername() throws Exception {
        SignUpDTO invalidSignUpDTO = new SignUpDTO(TOO_SHORT_CREDENTIAL, CORRECT_PASSWORD);
        requestRegistrationVerification(invalidSignUpDTO, status().isConflict());
    }

    @Test
    public void registerUserWithTooLongPassword() throws Exception {
        SignUpDTO invalidSignUpDTO = new SignUpDTO(CORRECT_USERNAME, TOO_LONG_PASSWORD);
        requestRegistrationVerification(invalidSignUpDTO, status().isConflict());
    }

    @Test
    public void registerUserWithTooShortPassword() throws Exception {
        SignUpDTO invalidSignUpDTO = new SignUpDTO(CORRECT_USERNAME, TOO_SHORT_CREDENTIAL);
        requestRegistrationVerification(invalidSignUpDTO, status().isConflict());
    }

    @Test
    public void registerUserWithNotPatternedUsername() throws Exception {
        SignUpDTO invalidSignUpDTO = new SignUpDTO(NOT_PATTERNED_USERNAME, CORRECT_PASSWORD);
        requestRegistrationVerification(invalidSignUpDTO, status().isConflict());
    }

    @Test
    public void registerUserWithNotPatternedPassword() throws Exception {
        SignUpDTO invalidSignUpDTO = new SignUpDTO(CORRECT_USERNAME, NOT_PATTERNED_PASSWORD);
        requestRegistrationVerification(invalidSignUpDTO, status().isConflict());
    }

    @Test
    public void registerUserWithCorrectCredentials() throws Exception {
        SignUpDTO validSignUpDTO = new SignUpDTO(CORRECT_USERNAME, CORRECT_PASSWORD);
        requestRegistrationVerification(validSignUpDTO, status().isOk());
    }

    @Test
    public void registerAlreadyRegisteredUser() throws Exception {
        SignUpDTO validSignUpDTO = new SignUpDTO(TEST_USERNAME, TEST_PASSWORD);
        requestRegistrationVerification(validSignUpDTO, status().isConflict());
    }
}
