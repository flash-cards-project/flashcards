// Cardset's name constants
export const MIN_NAME_LENGTH = 1;
export const MAX_NAME_LENGTH = 32;

// Cardset's complexity constants
export const MIN_COMPLEXITY = 1;
export const MAX_COMPLEXITY = 5;