// Card's question constants
export const MIN_QUESTION_LENGTH = 1;
export const MAX_QUESTION_LENGTH = 255;
export const QUESTION_LENGTH_VALID_MSG =
    `The question of card must be between ${MIN_QUESTION_LENGTH} and ${MAX_QUESTION_LENGTH} characters long.`;

// Card's answer constants
export const MIN_ANSWER_LENGTH = 1;
export const MAX_ANSWER_LENGTH = 255;
export const ANSWER_LENGTH_VALID_MSG =
    `The answer of card must be between ${MIN_ANSWER_LENGTH} and ${MAX_ANSWER_LENGTH} characters long.`;

// General card's constants
export const MIN_CARD_FIELD_LENGTH = MIN_QUESTION_LENGTH < MIN_ANSWER_LENGTH
    ? MIN_QUESTION_LENGTH
    : MIN_ANSWER_LENGTH;

export const MAX_CARD_FIELD_LENGTH = MAX_QUESTION_LENGTH > MAX_ANSWER_LENGTH
    ? MAX_QUESTION_LENGTH
    : MAX_ANSWER_LENGTH;