// Auth's username constants
export const MIN_USERNAME_LENGTH = 4;
export const MIN_USERNAME_LENGTH_VALID_MSG =
    `Username must be at least ${MIN_USERNAME_LENGTH} characters long.`;
export const MAX_USERNAME_LENGTH = 32;
export const MAX_USERNAME_LENGTH_VALID_MSG =
    `Username must be no longer than ${MAX_USERNAME_LENGTH} characters.`;
export const USERNAME_PATTERN = "^[a-zA-Z0-9]*$";
export const USERNAME_PATTERN_VALID_MSG = "The username must consist only of latin letters and digits.";

// Auth's password constants
export const MIN_PASSWORD_LENGTH = 4;
export const MIN_PASSWORD_LENGTH_VALID_MSG =
    `Password must be at least ${MIN_PASSWORD_LENGTH} characters long.`;
export const MAX_PASSWORD_LENGTH = 32;
export const MAX_PASSWORD_LENGTH_VALID_MSG =
    `Password must be no longer than ${MAX_PASSWORD_LENGTH} characters.`
export const PASSWORD_PATTERN = "^[a-zA-Z0-9?!@#$%^&+=_-]*$";
export const PASSWORD_PATTERN_VALID_MSG =
    "The password must consist only of latin letters, digits, and ?! @ # $ % ^ & + = _ -";

// General messages
export const REQUIRED_FIELD_MSG = "This field is required.";

// LoginPage's messages
export const INVALID_LOGIN_MSG = "Invalid username or password.";

// RegisterPage's messages
export const PASSWORDS_DONT_MATCH_VALID_MSG = "Passwords don't match.";