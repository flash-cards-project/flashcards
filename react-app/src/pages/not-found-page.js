import React, {Fragment} from "react";
import '../styles/not-found.css'
import {Helmet} from "react-helmet";

function NotFoundPage() {
    return (
        <Fragment>
            <Helmet>
                <title>Not Found</title>
            </Helmet>
            <div id="notfound">
                <div className="notfound">
                    <div className="notfound-404">
                        <h3>Oops! Page not found</h3>
                        <h1><span>4</span><span>0</span><span>4</span></h1>
                    </div>
                    <h2>we are sorry, but the page you requested was not found</h2>
                </div>
            </div>
        </Fragment>
    )
}

export default (NotFoundPage)