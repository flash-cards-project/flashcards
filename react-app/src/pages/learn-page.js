import {useHistory, useParams} from "react-router-dom";
import axios from "axios";
import {Fragment, useEffect, useRef, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft, faCheck, faTimes, faEyeSlash} from "@fortawesome/free-solid-svg-icons";
import {Helmet} from "react-helmet";

function LearnPage() {
    const {cardsetId} = useParams();
    const [card, setCard] = useState({id: 0, question: "", answer: ""});
    const cardAnswerDiv = useRef();
    const showAnswerButton = useRef();

    const history = useHistory();

    useEffect(() => {
        getNextCard("UNDEFINED");
    }, [])

    const getNextCard = (levelOfKnowledge) => {
        axios.get(`/api/cardsets/${cardsetId}/nextcard`, {
            params: {
                cardId: card.id,
                level: levelOfKnowledge
            }
        })
            .then((response) => response.data)
            .then((data) => setCard(data))
    }

    const showAnswer = () => {
        showAnswerButton.current.style.display = "none";
        cardAnswerDiv.current.textContent = card.answer;
    }

    const hideAnswer = () => {
        showAnswerButton.current.style.display = null;
        cardAnswerDiv.current.textContent = "";
    }

    const submitAnswer = (levelOfKnowledge) => {
        hideAnswer();
        getNextCard(levelOfKnowledge);
    }

    const goBack = () => {
        axios.get(`/api/cardsets/${cardsetId}/stop_learn_session`)
            .then((response) => {
                history.push("/cardsets");
            });
    }

    return (
        <Fragment>
            <Helmet>
                <title>Learn</title>
            </Helmet>
            <div>
                <button id={"back-btn"} className={"btn primary-btn"} onClick={goBack}>
                    <FontAwesomeIcon icon={faArrowLeft}/> Back
                </button>
                <div className={"card-n-buttons-box"}>
                    <div className={"card-box"}>
                        <div className={"card-box-half"}>
                            <div className={"card-box-text-wrapper"}>
                                <div className={"card-box-text"}>
                                    <div>
                                        {card.question}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"card-box-half"}>
                            <div className={"card-box-text-wrapper"}>
                                <div className={"card-box-text"}>
                                    <div>
                                        <button ref={showAnswerButton}
                                                id={"show-answer-btn"} className={"btn gray-btn"}
                                                onClick={showAnswer}>
                                            <FontAwesomeIcon icon={faEyeSlash}/>
                                        </button>
                                        <div ref={cardAnswerDiv} className={"card-answer"}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={"btn-box"}>
                        <div className={"learn-button"}>
                            <button id={"bad-answer-btn"} className={"btn"} onClick={() => {
                                submitAnswer("BAD")
                            }}>
                                <FontAwesomeIcon icon={faTimes}/>
                            </button>
                            <output>DON'T KNOW</output>
                        </div>
                        <div className={"learn-button"}>
                            <button id={"correct-answer-btn"} className={"btn"} onClick={() => {
                                submitAnswer("GOOD")
                            }}>
                                <FontAwesomeIcon icon={faCheck}/>
                            </button>
                            <output>KNOW</output>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default (LearnPage);