import React, {Fragment, useRef} from "react";
import "../styles/auth.css"
import axios from "axios";
import {useHistory} from "react-router-dom";
import {
    USERNAME_PATTERN,
    MAX_USERNAME_LENGTH,
    MIN_USERNAME_LENGTH,
    PASSWORD_PATTERN,
    MAX_PASSWORD_LENGTH,
    MIN_PASSWORD_LENGTH,
    USERNAME_PATTERN_VALID_MSG,
    REQUIRED_FIELD_MSG,
    MIN_USERNAME_LENGTH_VALID_MSG,
    MAX_USERNAME_LENGTH_VALID_MSG,
    PASSWORD_PATTERN_VALID_MSG,
    MIN_PASSWORD_LENGTH_VALID_MSG, MAX_PASSWORD_LENGTH_VALID_MSG, PASSWORDS_DONT_MATCH_VALID_MSG
} from "../util/constants/auth-constants";
import {Helmet} from "react-helmet";

function RegisterPage() {
    const usernameInput = useRef();
    const passwordInput = useRef();
    const passwordConfirmInput = useRef();
    const errorLine = useRef();

    const history = useHistory();

    const printResponseMsgInErrorLine = (isError, message) => {
        errorLine.current.style.backgroundColor = isError === true ? "#F93154" : "#00B74A";
        errorLine.current.style.display = "block";
        errorLine.current.innerText = message;
    }

    const clearMessage = (event) => {
        event.target.setCustomValidity("");
        errorLine.current.style.display = "none";
    }

    const checkUsernameValidity = () => {
        let error = "";
        const username = usernameInput.current.value;

        if (!username.match(USERNAME_PATTERN)) {
            error = USERNAME_PATTERN_VALID_MSG;
        } else if (username.length === 0) {
            error = REQUIRED_FIELD_MSG;
        } else if (username.length < MIN_USERNAME_LENGTH) {
            error = MIN_USERNAME_LENGTH_VALID_MSG;
        } else if (username.length > MAX_USERNAME_LENGTH) {
            error = MAX_USERNAME_LENGTH_VALID_MSG;
        }

        usernameInput.current.setCustomValidity(error);
        return usernameInput.current.reportValidity();
    }

    const checkPasswordValidity = () => {
        let error = "";
        const password = passwordInput.current.value;

        if (!password.match(PASSWORD_PATTERN)) {
            error = PASSWORD_PATTERN_VALID_MSG;
        } else if (password.length === 0) {
            error = REQUIRED_FIELD_MSG;
        } else if (password.length < MIN_PASSWORD_LENGTH) {
            error = MIN_PASSWORD_LENGTH_VALID_MSG;
        } else if (password.length > MAX_PASSWORD_LENGTH) {
            error = MAX_PASSWORD_LENGTH_VALID_MSG;
        }

        passwordInput.current.setCustomValidity(error);
        return passwordInput.current.reportValidity();
    }

    const checkPasswordsConfirmValidity = () => {
        let error = "";
        const password = passwordInput.current.value;
        const passwordConfirm = passwordConfirmInput.current.value;

        if (passwordConfirm.length === 0) {
            error = REQUIRED_FIELD_MSG;
        } else if (password.localeCompare(passwordConfirm)) {
            error = PASSWORDS_DONT_MATCH_VALID_MSG;
        }

        passwordConfirmInput.current.setCustomValidity(error);
        return passwordConfirmInput.current.reportValidity();
    }

    const submit = (event) => {
        event.preventDefault();
        if (checkUsernameValidity() && checkPasswordValidity() && checkPasswordsConfirmValidity()) {
            axios.post('/api/register', {
                username: usernameInput.current.value,
                password: passwordInput.current.value
            }, {})
                .then(() => {
                    printResponseMsgInErrorLine(false, "You have been successfully registered.");
                })
                .catch((error) => {
                    printResponseMsgInErrorLine(true, error.response.data.errors[0]);
                });
        }
    }

    const goToLoginPage = (event) => {
        event.preventDefault();
        history.push("/login");
    }

    return (
        <Fragment>
            <Helmet>
                <title>Register</title>
            </Helmet>
            <div className={"form-box"}>
                <div className={"form-title"}>
                    Create Account
                </div>
                <div className={"form-body"}>
                    <form>
                        <div className={"form-field"}>
                            <div className={"field-name"}>
                                Username
                            </div>
                            <input ref={usernameInput} type={"text"} className={"input"}
                                   autoFocus={true} onInput={clearMessage}/>
                        </div>
                        <div className={"form-field"}>
                            <div className={"field-name"}>
                                Password
                            </div>
                            <input ref={passwordInput} type={"password"} className={"input"}
                                   onInput={clearMessage}/>
                        </div>
                        <div className={"form-field"}>
                            <div className={"field-name"}>
                                Confirm password
                            </div>
                            <input ref={passwordConfirmInput} type={"password"} className={"input"}
                                   onInput={clearMessage}/>
                        </div>
                        <div ref={errorLine} className={"error-line"}/>
                        <button className={"form-btn"} onClick={submit}>
                            Sign Up
                        </button>
                    </form>
                    <div className={"form-advice"}>
                        Already registered?
                        <br/>
                        <a onClick={goToLoginPage} href={"login"}>Sign In</a>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default (RegisterPage);