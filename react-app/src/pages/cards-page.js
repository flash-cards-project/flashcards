import React, {Fragment, useEffect, useReducer, useRef} from "react";
import Navbar from "../components/navbar";
import SearchBar from "../components/search-bar"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faArrowLeft,
    faCheck,
    faSignOutAlt,
    faTimes,
    faTrash
} from "@fortawesome/free-solid-svg-icons";

import "../styles/cards.css"
import CardCreator from "../components/card-creator";
import CardTile from "../components/card-tile";
import {Map} from "immutable";
import axios from "axios";
import {useHistory, useParams} from "react-router-dom";
import ModalWindow from "../components/modal-window";
import {MAX_CARD_FIELD_LENGTH} from "../util/constants/card-constants";
import {MAX_COMPLEXITY, MIN_COMPLEXITY} from "../util/constants/cardset-constants";
import {Helmet} from "react-helmet";

const initState = Map({
    // id, cardsetId, question, answer, tier
    cards: [],
    cardset: {
        id: -1,
        name: "",
        complexity: 1
    },
    search_query: {
        query: "",
        level: 0
    }
});

function reducer(state, action) {
    switch (action.type) {
        case 'set_card':
            return state.update('cards', () => action.payload);
        case 'del_card':
            return state.update('cards', (c) => c.filter((c) => c.id !== action.payload.id));
        case 'add_card':
            return state.update('cards', (c) => c.concat(action.payload));
        case 'rename_card':
            return state.update('cards', (c) => {
                let index = c.map((x) => x.id).indexOf(action.payload.id);
                c[index].question = action.payload.question;
                c[index].answer = action.payload.answer;
                return c;
            });
        case 'set_cardset':
            return state.update('cardset', () => action.payload);
        case 'change_search_query':
            return state.update('search_query', () => action.payload);
        default:
            return state;
    }
}

function CardsPage() {
    let cardToDel = null;

    const [state, dispatch] = useReducer(reducer, initState);
    const params = useParams();
    const complexityRange = useRef();
    const complexitySaveButtons = useRef();
    const compOutputRange = useRef();
    const searchSelect = useRef();
    const searchAnswerQuestionInput = useRef();
    const modalWindow = useRef();
    const searchBar = useRef();
    const cardCreator = useRef();
    const complexityRangeBox = useRef();
    const cardTilesRefs = useRef([]);

    const history = useHistory();

    useEffect(() => {
        axios.get(`/api/cardsets/${params.id}`)
            .then((response) => response.data)
            .then((data) => {
                dispatch({type: 'set_cardset', payload: data});
                setCompSliderValue(data.complexity);
            })
            .catch(() => {
                history.push("/cardsets");
            });
    }, []);

    useEffect(() => {
        axios.get('/api/cards', {
            params: {
                cardsetId: params.id
            }
        })
            .then((response) => response.data)
            .then((data) =>
                dispatch({type: 'set_card', payload: data}))
            .catch(() => {
                history.push("/cardsets");
            });
    }, []);

    useEffect(() => {
        cardTilesRefs.current = cardTilesRefs.current.slice(0, state.get("cards").length);
    }, [state.get("cards").length])

    const cardState = (id) => {
        let getState = () => {
            let index = state.get('cards').map((x) => x.id).indexOf(id);
            return state.get('cards')[index];
        };
        return {
            getCopy() {
                return JSON.parse(JSON.stringify(getState()));
            },
            get id() {
                return getState().id;
            },
            get question() {
                return getState().question;
            },
            get answer() {
                return getState().answer;
            },
            get tier() {
                return getState().tier;
            }
        }
    }

    const setCompSliderValue = (value) => {
        complexityRange.current.value = value;
    };

    const delCard = (c) => {
        axios.delete(`/api/cards/${c.id}`, {
            data: {
                cardsetId: params.id
            }
        })
            .then(() => {
                dispatch({type: 'del_card', payload: c});
            })
    };

    const addCard = (c, onSuccess, onFail) => {
        axios.post('/api/cards', {
            cardsetId: params.id,
            question: c.question,
            answer: c.answer
        })
            .then((response) => response.data)
            .then((data) => {
                dispatch({type: 'add_card', payload: data});
                onSuccess();
            })
            .catch((error) => {
                let errMsg = "";
                if ("errors" in error.response.data) {
                    errMsg = error.response.data.errors[0];
                }
                onFail(errMsg);
            });
    };

    const changeCardQA = (c, onSuccess, onFail) => {
        axios.put(`/api/cards/${c.id}`, {
            cardsetId: params.id,
            question: c.question,
            answer: c.answer
        })
            .then(() => {
                dispatch({type: 'rename_card', payload: c});
                onSuccess(c.question, c.answer);
            })
            .catch((error) => {
                let errMsg = "";
                if ("errors" in error.response.data) {
                    errMsg = error.response.data.errors[0];
                }
                onFail(errMsg);
            });
    };

    const changeComplexity = (newCardsetComplexity) => {
        axios.put(`/api/cardsets/${params.id}/complexity`, {
            complexity: newCardsetComplexity
        })
            .then((response) => response.data)
            .then((data) => {
                dispatch({type: 'set_cardset', payload: data})
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const goBack = () => {
        history.push("/cardsets");
    }

    const changeRangeVal = () => {
        compOutputRange.current.value = complexityRange.current.value;
        if (parseInt(complexityRange.current.value) !== state.get("cardset").complexity) {
            setVisibilityForComplexitySaveButtons();
        } else {
            removeVisibilityForComplexitySaveButtons();
        }
    }

    const resetComplexityValue = () => {
        complexityRange.current.value = compOutputRange.current.value = state.get("cardset").complexity;
        removeVisibilityForComplexitySaveButtons();
    }

    const saveComplexityValue = () => {
        const newComplexityValue = parseInt(complexityRange.current.value);
        changeComplexity(newComplexityValue);
        removeVisibilityForComplexitySaveButtons();
    }

    const setVisibilityForComplexitySaveButtons = () => {
        complexitySaveButtons.current.style.visibility = "visible";
        if (state.get("cardset").complexity !== parseInt(complexityRange.current.value)) {
            complexitySaveButtons.current.style.opacity = 1;
        }
    }

    const removeVisibilityForComplexitySaveButtons = () => {
        complexitySaveButtons.current.style = null;
    }

    const enableComplexityRange = () => {
        if (complexityRangeBox.current.classList.contains("disabled")) {
            complexityRangeBox.current.classList.remove("disabled");
        }
        setVisibilityForComplexitySaveButtons();
    }

    const disableComplexityRange = () => {
        if (!complexityRangeBox.current.classList.contains("disabled")) {
            complexityRangeBox.current.classList.add("disabled");
        }
        removeVisibilityForComplexitySaveButtons();
    }

    const enableElementsVisibleForEditCardMode = () => {
        if (!isSearchMode()) {
            cardCreator.current.enableCardCreator();
            enableComplexityRange();
        }
        searchBar.current.enableSearchBar();
    }

    const disableElementsVisibleForEditCardMode = () => {
        cardCreator.current.disableCardCreator();
        disableComplexityRange();
        searchBar.current.disableSearchBar();
    }

    const enableNonActiveCardTiles = () => {
        for (let cardTile of cardTilesRefs.current) {
            if (!cardTile.isActive()) {
                cardTile.enableCardTile();
            }
        }
    }

    const disableNonActiveCardTiles = () => {
        for (let cardTile of cardTilesRefs.current) {
            if (!cardTile.isActive()) {
                cardTile.disableCardTile();
            }
        }
    }

    const selectChange = () => {
        searchSelect.current.style.color = parseInt(searchSelect.current.value) !== 0
            ? "black"
            : "gray";
    }

    const resetValidation = (event) => {
        event.target.setCustomValidity("");
    }

    const startSearching = () => {
        findCards();
        changeSearchState();
        reloadSearchMode();
    }

    const findCards = () => {
        axios.get(`/api/cards/search`, {
            params: {
                cardsetId: params.id,
                query: searchAnswerQuestionInput.current.value,
                level: parseInt(searchSelect.current.value)
            }
        })
            .then((response) => response.data)
            .then((data) =>
                dispatch({type: 'set_card', payload: data}))
            .catch((error) => {
                if (error.response.status === 403) {
                    history.push("/cardsets")
                } else {
                    searchAnswerQuestionInput.current.setCustomValidity(error.response.data);
                    searchAnswerQuestionInput.current.reportValidity();
                }
            });
    }

    const changeSearchState = () => {
        dispatch({
            type: 'change_search_query',
            payload: {
                query: searchAnswerQuestionInput.current.value,
                level: parseInt(searchSelect.current.value)
            }
        });
    }

    const reloadSearchMode = () => {
        searchAnswerQuestionInput.current.value !== "" || parseInt(searchSelect.current.value) !== 0
            ? setSearchMode()
            : removeSearchMode();
        searchBar.current.removeActiveMode();
    }

    const setSearchMode = () => {
        cardCreator.current.disableCardCreator();
        disableComplexityRange();
        searchBar.current.setResetButtonVisibility();
    }

    const removeSearchMode = () => {
        cardCreator.current.enableCardCreator();
        enableComplexityRange();
        searchBar.current.removeResetButtonVisibility();
    }

    const isSearchMode = () => {
        const searchState = state.get('search_query');
        return searchState.query !== '' || searchState.level !== 0;
    }

    const queryAndLevelInEachCards = (query, level) => {
        const allCards = state.get('cards');
        for (let card of allCards) {
            if ((card.question.indexOf(query) === -1 && card.answer.indexOf(query) === -1)
                || (level !== 0 && card.tier !== level)) {
                return false;
            }
        }
        return true;
    }

    const checkSearchQueryEquals = () => {
        const searchState = state.get('search_query');
        const searchQuery = searchAnswerQuestionInput.current.value;
        const searchLevel = parseInt(searchSelect.current.value);

        if (!isSearchMode()) {
            if (!queryAndLevelInEachCards(searchQuery, searchLevel)) {
                searchBar.current.setActiveMode();
            } else {
                searchBar.current.removeActiveMode();
            }
        } else {
            if (!queryAndLevelInEachCards(searchQuery, searchLevel)
                || searchQuery !== searchState.query
                || searchLevel !== searchState.level) {
                searchBar.current.setActiveMode();
            } else {
                searchBar.current.removeActiveMode();
            }
        }
    }

    const openDelOverlay = (c) => {
        cardToDel = c;
        modalWindow.current.setModalWindowContent(
            "Confirmation of deletion",
            `Are you sure that you want to delete card with question:\n"${
                c.question.length > 32
                    ? c.question.substr(0, 32) + "..."
                    : c.question}" ?\nThis action can not be undone.`,
            deleteCardModalWindowButtons);
        modalWindow.current.switchOverlay();
    }

    const closeDelOverlay = () => {
        cardToDel = null;
        modalWindow.current.switchOverlay();
    }

    const delCardAndCloseOverlay = () => {
        delCard(cardToDel);
        closeDelOverlay();
    }

    const deleteCardModalWindowButtons = [
        <button onClick={closeDelOverlay} className={"btn primary-btn"}>
            <FontAwesomeIcon icon={faSignOutAlt}/> Cancel
        </button>,
        <button onClick={delCardAndCloseOverlay} id={"danger"} className={"btn" +
        " secondary-btn"}>
            <FontAwesomeIcon icon={faTrash}/> Delete
        </button>
    ];

    const searchBarElements = [
        <select ref={searchSelect} className={"select"} onInput={() => {
            selectChange();
            checkSearchQueryEquals();
        }}>
            <option value={0}>Any level</option>
            <option value={1}>1</option>
            <option value={2}>2</option>
            <option value={3}>3</option>
            <option value={4}>4</option>
            <option value={5}>5</option>
        </select>,
        <input ref={searchAnswerQuestionInput} className={"input question-answer-search-input"}
               placeholder={"Search for answer and question..."} maxLength={MAX_CARD_FIELD_LENGTH}
               onInput={(event) => {
                   resetValidation(event);
                   checkSearchQueryEquals();
               }}/>
    ];

    const navBarElements = [
        <button id={"light"} className={"btn back-button"} onClick={goBack}>
            <FontAwesomeIcon icon={faArrowLeft}/> Back
        </button>,
        <div style={{fontSize: 18}}>
            {state.get("cardset").name}
        </div>,
        <div ref={complexityRangeBox} id={"complexity-range"} className={"complexity-box"}>
            <div className={"range-title"}>
                Complexity
            </div>
            <output ref={compOutputRange} id={"complexity-range-value"}>
                {state.get("cardset").complexity}
            </output>
            <input ref={complexityRange} type={"range"} min={MIN_COMPLEXITY} max={MAX_COMPLEXITY}
                   onInput={changeRangeVal}/>
            <div ref={complexitySaveButtons} className={"save-complexity-buttons"}>
                <button id={"success"} className={"btn"} onClick={saveComplexityValue}>
                    <FontAwesomeIcon icon={faCheck}/>
                </button>
                <br/>
                <button id={"danger"} className={"btn"} onClick={resetComplexityValue}>
                    <FontAwesomeIcon icon={faTimes}/>
                </button>
            </div>
        </div>
    ];

    return (
        <Fragment>
            <Helmet>
                <title>Cards</title>
            </Helmet>
            <ModalWindow ref={modalWindow}/>
            <Navbar elements={navBarElements}/>
            <SearchBar ref={searchBar} elements={searchBarElements} onSearch={startSearching}/>
            <div className={"card-tile-grid"}>
                <CardCreator ref={cardCreator} onAdd={addCard}/>
                {
                    state.get('cards').sort((a, b) => (b.id - a.id)).map((c, i) => (
                        <CardTile key={c.id} ref={element => cardTilesRefs.current[i] = element}
                                  cardState={cardState(c.id)}
                                  onDelOverlay={openDelOverlay}
                                  onQAChange={changeCardQA}
                                  checkSearchQueryEqualsAfterRename={checkSearchQueryEquals}
                                  editCardFuncs={{
                                      enableElementsVisibleForEditCardMode: enableElementsVisibleForEditCardMode,
                                      disableElementsVisibleForEditCardMode: disableElementsVisibleForEditCardMode,
                                      enableNonActiveCardTiles: enableNonActiveCardTiles,
                                      disableNonActiveCardTiles: disableNonActiveCardTiles
                                  }}/>
                    ))
                }
            </div>
        </Fragment>
    );
}

export default (CardsPage);