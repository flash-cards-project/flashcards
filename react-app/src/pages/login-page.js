import React, {Fragment, useRef} from "react";
import "../styles/auth.css"
import axios from "axios";
import Cookies from "js-cookie";
import {useHistory} from "react-router-dom";
import {
    USERNAME_PATTERN,
    MAX_USERNAME_LENGTH,
    MIN_USERNAME_LENGTH,
    PASSWORD_PATTERN,
    MAX_PASSWORD_LENGTH,
    MIN_PASSWORD_LENGTH,
    INVALID_LOGIN_MSG,
    REQUIRED_FIELD_MSG
} from "../util/constants/auth-constants";
import {Helmet} from "react-helmet";

function LoginPage() {
    const usernameInput = useRef();
    const passwordInput = useRef();
    const errorLine = useRef();

    const history = useHistory();

    const printMsgInErrorLine = (message) => {
        errorLine.current.style.display = "block";
        errorLine.current.innerText = message;
    }

    const clearMessage = (event) => {
        event.target.setCustomValidity("");
        errorLine.current.style.display = "none";
    }

    const checkRequired = () => {
        if (usernameInput.current.value.length === 0) {
            usernameInput.current.setCustomValidity(REQUIRED_FIELD_MSG);
        } else if (passwordInput.current.value.length === 0) {
            passwordInput.current.setCustomValidity(REQUIRED_FIELD_MSG);
        }
        return usernameInput.current.reportValidity() && passwordInput.current.reportValidity();
    }

    const checkUsernameValidity = () => {
        const username = usernameInput.current.value;
        if (!username.match(USERNAME_PATTERN)
            || username.length < MIN_USERNAME_LENGTH
            || username.length > MAX_USERNAME_LENGTH) {
            printMsgInErrorLine(INVALID_LOGIN_MSG);
            return false;
        }
        return true;
    }

    const checkPasswordValidity = () => {
        const password = passwordInput.current.value;
        if (!password.match(PASSWORD_PATTERN)
            || password.length < MIN_PASSWORD_LENGTH
            || password.length > MAX_PASSWORD_LENGTH) {
            printMsgInErrorLine(INVALID_LOGIN_MSG);
            return false;
        }
        return true;
    }

    const submit = (event) => {
        event.preventDefault();
        if (checkRequired() && checkUsernameValidity() && checkPasswordValidity()) {
            axios.get('/api/login', {
                auth: {
                    username: usernameInput.current.value,
                    password: passwordInput.current.value
                }
            })
                .then((response) => {
                    if (response.status === 200) {
                        Cookies.set("user", usernameInput.current.value);
                        history.push("/cardsets");
                    }
                })
                .catch((error) => {
                    if (error.response.status === 401) {
                        printMsgInErrorLine("Invalid username or password.");
                    }
                });
        }
    }

    const goToRegisterPage = (event) => {
        event.preventDefault();
        history.push("register")
    }

    return (
        <Fragment>
            <Helmet>
                <title>Login</title>
            </Helmet>
            <div className={"form-box"}>
                <div className={"form-title"}>
                    Sign In
                </div>
                <div className={"form-body"}>
                    <form>
                        <div className={"form-field"}>
                            <div className={"field-name"}>
                                Username
                            </div>
                            <input ref={usernameInput} type={"text"} className={"input"}
                                   autoFocus={true} onInput={clearMessage}/>
                        </div>
                        <div className={"form-field"}>
                            <div className={"field-name"}>
                                Password
                            </div>
                            <input ref={passwordInput} type={"password"} className={"input"}
                                   onInput={clearMessage}/>
                        </div>
                        <div ref={errorLine} className={"error-line"}/>
                        <button className={"form-btn"} onClick={submit}>
                            Log in
                        </button>
                    </form>
                    <div className={"form-advice"}>
                        Don't have an account?
                        <br/>
                        <a onClick={goToRegisterPage} href={"register"}>Sign Up</a>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default (LoginPage);