import React, {Fragment, useEffect, useReducer, useRef} from "react";
import {Map as ImmutableMap} from 'immutable';
import '../styles/start.css';
import '../styles/global.css'
import Navbar from "../components/navbar";
import axios from "axios";
import CardsetTile from "../components/cardset-tile";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusSquare, faSignOutAlt, faTrash, faUndoAlt} from "@fortawesome/free-solid-svg-icons";
import Cookies from "js-cookie";
import ModalWindow from "../components/modal-window";
import SearchBar from "../components/search-bar";
import {useHistory} from "react-router-dom";
import {MAX_NAME_LENGTH} from "../util/constants/cardset-constants";
import {Helmet} from "react-helmet";

const initState = ImmutableMap({
    cardsets: [],
    cardsets_learn_stats: ImmutableMap(),
    search_name_query: ""
});

function reducer(state, action) {
    switch (action.type) {
        case 'set_cardsets':
            return state.update('cardsets', () => action.payload);
        case 'del_cardset':
            return state.update('cardsets', (c) => c.filter((c) => c.id !== action.payload.id));
        case 'add_cardset':
            return state.update('cardsets', (c) => c.concat(action.payload));
        case 'rename_cardset':
            return state.update('cardsets', (c) => {
                let index = c.map((x) => x.id).indexOf(action.payload.id);
                c[index].name = action.payload.name;
                return c;
            });
        /*case 'add_cardset_learn_stat':
            return state.update('cardsets_learn_stats', (c) => c.set(action.payload.id, action.payload.stats));*/
        case 'change_search_name_query':
            return state.update('search_name_query', () => action.payload);
        default:
            return state;
    }
}

function StartPage() {
    const [state, dispatch] = useReducer(reducer, initState);

    const modalWindow = useRef();
    const searchBar = useRef();
    const newCardsetButton = useRef();
    const logoutButton = useRef();
    const searchCardsetNameInput = useRef();
    const cardsetTilesRefs = useRef([]);

    const history = useHistory();

    useEffect(() => {
        axios.get('/api/cardsets')
            .then((response) => response.data)
            .then((data) =>
                dispatch({type: 'set_cardsets', payload: data}))
            .catch(() => {
                Cookies.set("user", "");
                history.push("/login");
            });
    }, []);

    useEffect(() => {
        cardsetTilesRefs.current = cardsetTilesRefs.current.slice(0, state.get("cardsets").length);
    }, [state.get("cardsets").length]);

    const cardsetState = (id) => {
        let getState = () => {
            let index = state.get('cardsets').map((x) => x.id).indexOf(id);
            return state.get('cardsets')[index];
        };
        return {
            getCopy() {
                return JSON.parse(JSON.stringify(getState()));
            },
            get id() {
                return getState().id;
            },
            get name() {
                return getState().name;
            },
            get complexity() {
                return getState().complexity;
            },
            get numOfCards() {
                return getState().numOfCards;
            },
            get progress() {
                return getState().progress;
            }
        }
    }

    const delCardset = (c) => {
        axios.delete(`/api/cardsets/${c.id}`)
            .then(() => {
                dispatch({type: 'del_cardset', payload: c});
            })
    };

    const addCardset = () => {
        axios.post('/api/cardsets/')
            .then((response) => response.data)
            .then((data) => {
                dispatch({type: 'add_cardset', payload: data});
            })
            .then(() => {
                cardsetTilesRefs.current[0].selectNewCardsetTileTitle();
            });
    };

    const returnOldNameToCardset = (id, setName) => {
        let oldName = cardsetState(id).name;
        setName(oldName);
    };

    const enableElementsVisibleForRenameCardsetMode = () => {
        if (!isSearchMode()) {
            enableNewCardsetButton();
        }
        enableLogoutButton();
        searchBar.current.enableSearchBar();
    }

    const disableElementsVisibleForRenameCardsetMode = () => {
        disableNewCardsetButton();
        disableLogoutButton();
        searchBar.current.disableSearchBar();
    }

    const disableNonActiveCardsetTiles = () => {
        for (let cardsetTile of cardsetTilesRefs.current) {
            if (!cardsetTile.isActive()) {
                cardsetTile.disableCardsetTile();
            }
        }
    }

    const enableNonActiveTiles = () => {
        for (let cardsetTile of cardsetTilesRefs.current) {
            if (!cardsetTile.isActive()) {
                cardsetTile.enableCardsetTile();
            }
        }
    }

    const renameCardset = (id, newName, onSuccess, onFail) => {
        let cardState = cardsetState(id).getCopy();
        cardState.name = newName;

        axios.put(`/api/cardsets/${cardState.id}`, cardState)
            .then((response) => response.data)
            .then((data) => {
                dispatch({type: 'rename_cardset', payload: data});
                onSuccess();
            })
            .catch((error) => {
                let errMsg = "";
                if ("errors" in error.response.data) {
                    errMsg = error.response.data.errors[0];
                }
                onFail(errMsg);
            });
    };

    let cardsetToDel;
    const openDelOverlay = (c) => {
        cardsetToDel = c;
        modalWindow.current.setModalWindowContent(
            "Confirmation of deletion",
            `Are you sure that you want to delete cardset with name "${c.name}"?\n`
                + `All cards in this cardset will be deleted.\nThis action can not be undone.`,
            deleteCardsetModalWindowButtons)
        modalWindow.current.switchOverlay();
    }

    const closeDelOverlay = () => {
        cardsetToDel = null;
        modalWindow.current.switchOverlay();
    }

    /* Cardset Progress stats */
    /*const getCardsetStat = (cardsetId, onSuccess, onFail) => {
        axios.get(`/api/cardsets/${cardsetId}/learn_stats`)
            .then((response) => response.data)
            .then((data) => {
                data = convertStatData(data.stats);
                const payload = {id: cardsetId, stats: data}
                dispatch({type: 'add_cardset_learn_stat', payload: payload});
                onSuccess(data);
            })
            .catch(() => {
                onFail();
            });
    }*/

    /*const convertStatData = (data) => {
        return Object.keys(data)
            .map((time) => [new Date(time), data[time]])
            .sort((a, b) => {
                if (a <= b) return 1;
                if (a > b) return -1;
            });
    }*/


    /*const openStatOverlay = (cardsetId) => {
        const setGraphAndOpen = (data) => {
            const graph = (
                <Chart
                    width={'800px'}
                    height={'600px'}
                    chartType="LineChart"
                    loader={<div>Loading Chart</div>}
                    data={[
                        ['x', 'progress'],
                        ...data
                    ]}
                    options={{
                        legend: {position: 'none'},
                        interpolateNulls: true,
                        hAxis: {
                            title: 'Time',
                            viewWindowMode: 'pretty'
                        },
                        vAxis: {
                            minValue: 0,
                            maxValue: 100,
                            title: 'Progress, %',
                        },
                    }}
                    rootProps={{'data-testid': '1'}}
                />
            );
            modalWindow.current.setModalWindowContent(
                "Cardset learning history", graph,
                statModalWindowButtons);
            modalWindow.current.switchOverlay();
        };

        let data;
        if (state.get('cardsets_learn_stats').has(cardsetId)) {
            data = state.get('cardsets_learn_stats').get(cardsetId);
            setGraphAndOpen(data);
        } else {
            getCardsetStat(cardsetId, setGraphAndOpen, () => {
            });
        }
    }*/

    const openStatOverlay = (cardsetId) => {
        const getStats = (onSuccess, onFail) => {
            axios.get(`/api/cardsets/${cardsetId}/learn_sessions`)
                .then((response) => response.data)
                .then((data) => {
                    // const payload = {id: cardsetId, stats: data}
                    // dispatch({type: 'add_cardset_learn_stat', payload: payload});
                    data.learnSessions.map((d) => {
                        d.startTime = new Date(d.startTime);
                        d.stopTime = new Date(d.stopTime);
                    });
                    onSuccess(data);
                })
                .catch(() => {
                    onFail();
                });
        };

        const displayModalWindow = (data) => {
            const name = cardsetState(data.cardsetId).name;
            data = data.learnSessions;

            const fmt = (d) => `${d.toLocaleDateString()}\n${d.toLocaleTimeString()}`;
            const minutes = (a, b) => {
                let diff = Math.abs(a.getTime() - b.getTime()) / 1000;
                diff /= 60;
                return Math.round(diff);
            }
            const table =
                data.length === 0 ? "No sessions found!" : (
                    <div className={"table-box"}>
                        <table className={"learn-session-table"}>
                            <thead>
                            <tr>
                                <th>Start time</th>
                                <th>Stop time</th>
                                <th>Duration, min</th>
                                <th>Marked as KNOWN</th>
                                <th>Marked as UNKNOWN</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                data.sort((a, b) => (b.startTime - a.startTime)).map((d) => (
                                    <tr>
                                        <td>{fmt(d.startTime)}</td>
                                        <td>{fmt(d.stopTime)}</td>
                                        <td>{minutes(d.startTime, d.stopTime)}</td>
                                        <td>{d.goodCount}</td>
                                        <td>{d.badCount}</td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
                );

            modalWindow.current.setModalWindowContent(
                `Cardset "${name}" learning sessions`, table,
                statModalWindowButtons);
            modalWindow.current.switchOverlay();
        };

        getStats(displayModalWindow, () => {});
    }

    const closeStatOverlay = () => {
        modalWindow.current.switchOverlay();
    }

    const logOut = () => {
        axios.get('/api/logout', {})
            .then((response) => {
                if (response.status === 200) {
                    Cookies.set("user", "");
                    history.push("/login");
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    const delCardsetAndCloseOverlay = () => {
        delCardset(cardsetToDel);
        closeDelOverlay();
    }

    const navBarElements = [
        <button ref={newCardsetButton} id={"new-cardset-btn"} className={"btn primary-btn"}
                onClick={addCardset}>
            <FontAwesomeIcon icon={faPlusSquare}/> New cardset
        </button>,
        <div>
            <div style={{fontSize: 18}}>
                Flashcards
            </div>
            <div style={{fontSize: 14}}>
                {Cookies.get("user")}
            </div>
        </div>,
        <button ref={logoutButton} id={"light"} className={"btn secondary-btn"}
                onClick={logOut}>
            <FontAwesomeIcon icon={faSignOutAlt}/> Logout
        </button>
    ];

    const deleteCardsetModalWindowButtons = [
        <button onClick={closeDelOverlay} className={"btn primary-btn"}>
            <FontAwesomeIcon icon={faSignOutAlt}/> Cancel
        </button>,
        <button onClick={delCardsetAndCloseOverlay} id={"danger"} className={"btn" +
        " secondary-btn"}>
            <FontAwesomeIcon icon={faTrash}/> Delete
        </button>
    ];

    const statModalWindowButtons = [
        <button onClick={closeStatOverlay} className={"btn primary-btn"}>
            <FontAwesomeIcon icon={faUndoAlt}/> Go back
        </button>
    ];

    const resetValidation = (event) => {
        event.target.setCustomValidity("");
    }

    const startSearching = () => {
        findCardsets();
        changeSearchState();
        reloadSearchMode();
    }

    const findCardsets = () => {
        axios.get(`/api/cardsets/search`, {
            params: {
                query: searchCardsetNameInput.current.value,
            }
        })
            .then((response) => response.data)
            .then((data) => {
                dispatch({type: 'set_cardsets', payload: data})
            })
            .catch((error) => {
                if (error.response.status === 403) {
                    history.push("/cardsets")
                } else {
                    searchCardsetNameInput.current.setCustomValidity(error.response.data);
                    searchCardsetNameInput.current.reportValidity();
                }
            });
    }

    const changeSearchState = () => {
        dispatch({type: 'change_search_name_query', payload: searchCardsetNameInput.current.value});
    }

    const reloadSearchMode = () => {
        searchCardsetNameInput.current.value !== "" ? setSearchMode() : removeSearchMode();
        searchBar.current.removeActiveMode();
    }

    const enableNewCardsetButton = () => {
        if (newCardsetButton.current.classList.contains("disabled")) {
            newCardsetButton.current.classList.remove("disabled");
        }
    }

    const disableNewCardsetButton = () => {
        if (!newCardsetButton.current.classList.contains("disabled")) {
            newCardsetButton.current.classList.add("disabled");
        }
    }

    const enableLogoutButton = () => {
        if (logoutButton.current.classList.contains("disabled")) {
            logoutButton.current.classList.remove("disabled");
        }
    }

    const disableLogoutButton = () => {
        if (!logoutButton.current.classList.contains("disabled")) {
            logoutButton.current.classList.add("disabled");
        }
    }

    const setSearchMode = () => {
        disableNewCardsetButton();
        searchBar.current.setResetButtonVisibility();
    }

    const removeSearchMode = () => {
        enableNewCardsetButton();
        searchBar.current.removeResetButtonVisibility();
    }

    const isSearchMode = () => {
        return state.get('search_name_query').length !== 0;
    }

    const queryInEachCardsetName = (query) => {
        const allCardsets = state.get('cardsets');
        for (let cardset of allCardsets) {
            if (cardset.name.indexOf(query) === -1) {
                return false;
            }
        }
        return true;
    }

    const checkSearchQueryEquals = () => {
        const searchState = state.get('search_name_query');
        const searchQuery = searchCardsetNameInput.current.value;

        if (!isSearchMode()) {
            if (!queryInEachCardsetName(searchQuery)) {
                searchBar.current.setActiveMode();
            } else {
                searchBar.current.removeActiveMode();
            }
        } else {
            if (!queryInEachCardsetName(searchQuery) || searchQuery !== searchState) {
                searchBar.current.setActiveMode();
            } else {
                searchBar.current.removeActiveMode();
            }
        }
    }

    const searchBarElements = [
        <input ref={searchCardsetNameInput} className={"input cardset-name-search-input"}
               placeholder={"Search for cardset name..."} maxLength={MAX_NAME_LENGTH}
               onInput={(event) => {
                   resetValidation(event);
                   checkSearchQueryEquals();
               }}/>
    ];

    return (
        <Fragment>
            <Helmet>
                <title>Cardsets</title>
            </Helmet>
            <ModalWindow ref={modalWindow}/>
            <Navbar elements={navBarElements}/>
            <SearchBar ref={searchBar} elements={searchBarElements} onSearch={startSearching}/>
            <div className={"cardset-tile-grid"}>
                {
                    state.get('cardsets').sort((a, b) => (b.id - a.id)).map((c, i) => (
                        <div className={"cardset-tile-grid-el"}>
                            <CardsetTile ref={element => cardsetTilesRefs.current[i] = element}
                                         key={c.id}
                                         cardsetState={cardsetState(c.id)}
                                         checkSearchQueryEqualsAfterRename={checkSearchQueryEquals}
                                         onRename={renameCardset} onDelOverlay={openDelOverlay}
                                         onStatOverlay={openStatOverlay}
                                         onOldNameReturn={returnOldNameToCardset}
                                         renameCardsetFuncs={{
                                             enableElementsVisibleForRenameCardsetMode,
                                             disableElementsVisibleForRenameCardsetMode,
                                             enableNonActiveTiles,
                                             disableNonActiveCardsetTiles,
                                         }}/>
                        </div>
                    ))
                }
            </div>
        </Fragment>
    );
}

export default (StartPage);