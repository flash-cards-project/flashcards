import './App.css';
import StartPage from "./pages/start-page";
import './styles/global.css';
import './styles/start.css'
import './styles/learn.css'

import LoginPage from "./pages/login-page";
import RegisterPage from "./pages/register-page";
import NotFoundPage from "./pages/not-found-page";
import CardsPage from "./pages/cards-page";
import {HashRouter, Redirect, Route, Switch} from "react-router-dom";
import Cookies from "js-cookie";
import LearnPage from "./pages/learn-page";

function AppRoute({path, component}) {
    try {
        if (component === LoginPage || component === RegisterPage) {
            if (Cookies.get("user").length)
                return <Redirect to={"/cardsets"}/>;
        } else {
            if (!Cookies.get("user").length)
                return <Route component={NotFoundPage}/>;
        }
        return <Route path={path} component={component}/>;
    } catch (error) {
        Cookies.set("user", "");
        return <Redirect to={"/login"}/>;
    }
}

function App() {
    return (
        <HashRouter>
            <Switch>
                <AppRoute exact path={"/login"} component={LoginPage}/>
                <AppRoute exact path={"/register"} component={RegisterPage}/>
                <AppRoute exact path={"/cardsets"} component={StartPage}/>
                <AppRoute exact path={"/cardsets/:id/cards"} component={CardsPage}/>
                <AppRoute exact path={"/cardsets/:cardsetId/learn"} component={LearnPage}/>
                <Redirect exact from={"/"} to={"/login"}/>
                <Route path={"*"} component={NotFoundPage}/>
            </Switch>
        </HashRouter>
    );
}

export default App;
