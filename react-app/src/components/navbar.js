import React from 'react'
import '../styles/start.css'

function Navbar(props) {
    const navBarElements = props.elements.map((element) =>
        <div className={"nav-element"}>
            {element}
        </div>
    );
    return (
        <nav className="nav">
            {navBarElements}
        </nav>
    );
}

export default (Navbar);