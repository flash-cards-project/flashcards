import React, {forwardRef, Fragment, useImperativeHandle, useRef} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";

function CardCreator(props, ref) {
    const question = useRef();
    const answer = useRef();
    const creatorButtonsBox = useRef();

    useImperativeHandle(ref, () => ({
        enableCardCreator: enableCardCreator,
        disableCardCreator: disableCardCreator
    }));

    const enableCardCreatorButtons = () => {
        const creatorButtons = creatorButtonsBox.current.children;
        for (let button of creatorButtons) {
            if (button.classList.contains("disabled")) {
                button.classList.remove("disabled");
            }
        }
        if (question.current.value.length && answer.current.value.length) {
            creatorButtonsBox.current.style.visibility = "visible";
            creatorButtonsBox.current.style.opacity = "1";
        }
    }

    const disableCardCreatorButtons = () => {
        const creatorButtons = creatorButtonsBox.current.children;
        for (let button of creatorButtons) {
            if (!button.classList.contains("disabled")) {
                button.classList.add("disabled");
            }
        }
        creatorButtonsBox.current.style.visibility = "hidden";
        creatorButtonsBox.current.style.opacity = "0";
    }

    const enableCardCreatorTextAreas = () => {
        if (question.current.classList.contains("disabled")) {
            question.current.classList.remove("disabled");
        }
        if (answer.current.classList.contains("disabled")) {
            answer.current.classList.remove("disabled");
        }
    }

    const disableCardCreatorTextAreas = () => {
        if (!question.current.classList.contains("disabled")) {
            question.current.classList.add("disabled");
        }
        if (!answer.current.classList.contains("disabled")) {
            answer.current.classList.add("disabled");
        }
    }

    const enableCardCreator = () => {
        enableCardCreatorTextAreas();
        enableCardCreatorButtons();
    }

    const disableCardCreator = () => {
        disableCardCreatorTextAreas();
        disableCardCreatorButtons();
    }

    const resizeTextArea = (field) => {
        field.style.outlineColor = "#00C1FE";
        field.setCustomValidity("");
        enableCardCreatorButtons();
        field.style.height = "auto";
        field.style.height = (field.scrollHeight) + "px";
    }

    const revertChanges = () => {
        question.current.style.outlineColor = "#00C1FE";
        question.current.value = answer.current.value = "";
        resizeTextArea(question.current);
        resizeTextArea(answer.current);
        disableCardCreatorButtons();
    }

    const validateField = (field) => {
        let error = "";
        let value = field.value.trim();
        if (value.length === 0) {
            error = "This field can't be empty.";
        } else if (value.length > 255) {
            error = "Text in this field must be no longer than 255 characters.";
        }
        field.setCustomValidity(error);
        field.reportValidity();

        return error === "";
    }

    const addCard = () => {
        if (validateField(question.current) && validateField(answer.current)) {
            props.onAdd({
                question: question.current.value,
                answer: answer.current.value
            }, revertChanges, reportValidity);
        }
    }

    const reportValidity = (errMsg) => {
        question.current.setCustomValidity(errMsg);
        question.current.reportValidity();
        question.current.style.outlineColor = "red";
    }

    return (
        <Fragment>
            <div id={"card-tile-creator"} className={"card-tile-creator"}>
                <div className={"card-tile-half"}>
                    <div className={"card-tile-title"}>
                        Question
                    </div>
                    <textarea ref={question} id={"question"} rows="1" wrap="soft" maxLength={255}
                              className={"textarea card-tile-creator-textarea"}
                              placeholder={"Your creativity here..."}
                              onInput={(event) => resizeTextArea(event.target)}/>
                </div>
                <div className={"card-tile-half"}>
                    <div className={"card-tile-title"}>
                        Answer
                    </div>
                    <textarea ref={answer} id={"answer"} rows="1" wrap="soft" maxLength={255}
                              className={"textarea card-tile-creator-textarea"}
                              placeholder={"Your creativity here..."}
                              onInput={(event) => resizeTextArea(event.target)}/>
                </div>
                <div ref={creatorButtonsBox} className={"add-new-card-buttons"}>
                    <button id={"success"} className={"btn"} onClick={addCard}>
                        <FontAwesomeIcon icon={faCheck}/>
                    </button>
                    <br/>
                    <button id={"danger"} className={"btn"} onClick={revertChanges}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </button>
                </div>
            </div>
        </Fragment>
    );
}

export default forwardRef(CardCreator);