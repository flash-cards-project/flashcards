import React, {forwardRef, useImperativeHandle, useRef} from 'react'
import '../styles/start.css'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

function SearchBar(props, ref) {
    const searchBar = useRef();
    const searchButton = useRef();
    const resetButton = useRef();

    useImperativeHandle(ref, () => ({
        setActiveMode: () => setActiveMode(),
        removeActiveMode: () => removeActiveMode(),
        enableSearchBar: () => enableSearchBar(),
        disableSearchBar: () => disableSearchBar(),
        setResetButtonVisibility: () => setResetButtonVisibility(),
        removeResetButtonVisibility: () => removeResetButtonVisibility()
    }));

    const clearAllSearchBarElements = (event) => {
        event.preventDefault();
        searchBar.current.reset();
    }

    const resetSearchMode = (event) => {
        clearAllSearchBarElements(event);
        removeResetButtonVisibility();
        props.onSearch();
    }

    const setResetButtonVisibility = () => {
        resetButton.current.style.visibility = "visible";
        resetButton.current.style.opacity = 1;
    }

    const removeResetButtonVisibility = () => {
        resetButton.current.style.visibility = "hidden";
        resetButton.current.style.opacity = 0;
    }

    const enableSearchButton = () => {
        if (searchButton.current.classList.contains("disabled")) {
            searchButton.current.classList.remove("disabled");
        }
    }

    const disableSearchButton = () => {
        if (!searchButton.current.classList.contains("disabled")) {
            searchButton.current.classList.add("disabled");
        }
    }

    const enableSearchBar = () => {
        enableSearchButton();
        enableSearchBarElements();
    }

    const disableSearchBar = () => {
        removeActiveMode();
        disableSearchButton();
        disableSearchBarElements();
    }

    const enableSearchBarElements = () => {
        const searchBarElements =
            document.getElementsByClassName("search").item(0).children;
        for (let item of searchBarElements) {
            if (item.classList.contains("disabled")) {
                item.classList.remove("disabled");
            }
        }
    }

    const disableSearchBarElements = () => {
        const searchBarElements =
            document.getElementsByClassName("search").item(0).children;
        for (let item of searchBarElements) {
            if (!item.classList.contains("disabled")) {
                item.classList.add("disabled");
            }
        }
    }

    const setActiveMode = () => {
        if (!searchButton.current.classList.contains("active")) {
            searchButton.current.classList.add("active");
        }
        enableSearchButton();
    }

    const removeActiveMode = () => {
        if (searchButton.current.classList.contains("active")) {
            searchButton.current.classList.remove("active");
        }
        disableSearchButton();
    }

    return (
        <form ref={searchBar} className={"search-bar-wrapper"}>
            <div id={"search-bar"} className={"search"}>
                {props.elements}
                <button ref={searchButton} id={"secondary"} className={"btn disabled"}
                        onClick={(event) => {
                            event.preventDefault();
                            props.onSearch();
                        }}>
                    Search
                </button>
                <div>
                    <button ref={resetButton} id={"danger"}
                            className={"btn reset-search-btn"} onClick={resetSearchMode}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </button>
                </div>
            </div>
        </form>
    );
}

export default forwardRef(SearchBar);