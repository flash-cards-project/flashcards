import React, {forwardRef, Fragment, useEffect, useImperativeHandle, useRef} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faSignal, faTimes, faTrash} from "@fortawesome/free-solid-svg-icons";
import {
    ANSWER_LENGTH_VALID_MSG,
    MAX_ANSWER_LENGTH,
    MAX_QUESTION_LENGTH, MIN_ANSWER_LENGTH,
    MIN_QUESTION_LENGTH,
    QUESTION_LENGTH_VALID_MSG
} from "../util/constants/card-constants";

function CardTile(props, ref) {
    let isActive = false;

    const cardTile = useRef();
    const editButtons = useRef();
    const deleteCardButton = useRef();
    const questionField = useRef();
    const answerField = useRef();
    const cardTierValve = useRef();

    useImperativeHandle(ref, () => ({
        isActive: () => {
            return isActive
        },
        enableCardTile: enableCardTile,
        disableCardTile: disableCardTile
    }));

    useEffect(() => {
        cardTierValve.current.classList.add(`card-level-valve-${props.cardState.tier}`);
    });

    const resetFieldValidity = (event) => {
        event.target.setCustomValidity("");
        event.target.style.outlineColor = "#00C1FE";
        event.target.style.outlineStyle = "auto";
    }

    const removeVisibilityForDeleteCardButton = () => {
        deleteCardButton.current.style.visibility = "hidden";
        deleteCardButton.current.style.opacity = 0;
    }

    const setVisibilityForEditCardButtons = () => {
        editButtons.current.style.visibility = "visible";
        editButtons.current.style.opacity = 1;
    }

    const activateCardTile = () => {
        questionField.current.style.outlineStyle = answerField.current.style.outlineStyle = "auto";
    }

    const deactivateCardTile = () => {
        questionField.current.style.outlineStyle = answerField.current.style.outlineStyle = null;
    }

    const setEditMode = () => {
        if (deleteCardButton.current.style.visibility === "hidden") {
            return;
        }
        activateCardTile();

        removeVisibilityForDeleteCardButton();
        setVisibilityForEditCardButtons();

        isActive = true;
        props.editCardFuncs.disableNonActiveCardTiles();
        props.editCardFuncs.disableElementsVisibleForEditCardMode();
    }

    const removeEditMode = () => {
        deactivateCardTile();

        props.editCardFuncs.enableNonActiveCardTiles();
        props.editCardFuncs.enableElementsVisibleForEditCardMode();
        isActive = false;

        // Revert styles to initial state (save hoveration effect)
        editButtons.current.style = deleteCardButton.current.style = null;

        props.checkSearchQueryEqualsAfterRename();
    }

    const revertChanges = () => {
        removeEditMode();

        questionField.current.value = props.cardState.question;
        answerField.current.value = props.cardState.answer;

        // Will be set if had an error -> red_field.
        if (questionField.current.style.outlineColor !== "#00C1FE") {
            questionField.current.style.outlineColor = "#00C1FE";
        }
    }

    const enableCardTile = () => {
        cardTile.current.style.pointerEvents = null;
    }

    const disableCardTile = () => {
        cardTile.current.style.pointerEvents = "none";
    }

    // Renaming and validation
    const validateCardQuestion = () => {
        const question = questionField.current.value.trim();
        if (question.length < MIN_QUESTION_LENGTH || question.length > MAX_QUESTION_LENGTH) {
            questionField.current.setCustomValidity(QUESTION_LENGTH_VALID_MSG);
        }
        return questionField.current.reportValidity();
    }

    const validateCardAnswer = () => {
        const answer = answerField.current.value.trim();
        if (answer.length < MIN_ANSWER_LENGTH || answer.length > MAX_ANSWER_LENGTH) {
            answerField.current.setCustomValidity(ANSWER_LENGTH_VALID_MSG);
        }
        return answerField.current.reportValidity();
    }

    const QAChangeWrapper = () => {
        if (validateCardQuestion() && validateCardAnswer()) {
            props.onQAChange({
                id: props.cardState.id,
                question: questionField.current.value,
                answer: answerField.current.value
            }, setNewQA, reportValidity);
        }
    }

    const reportValidity = (errMsg) => {
        questionField.current.setCustomValidity(errMsg);
        questionField.current.reportValidity();
        questionField.current.style.outlineColor = "red";
        questionField.current.style.outlineStyle = "auto";
    }

    const setNewQA = (newQuestion, newAnswer) => {
        questionField.current.value = newQuestion;
        answerField.current.value = newAnswer;

        removeEditMode(questionField.current);
    }

    const delOverlayWrapper = () => {
        props.onDelOverlay({
            id: props.cardState.id,
            question: props.cardState.question
        });
    }

    return (
        <Fragment>
            <div ref={cardTile} className={"card-tile"}>
                <div className={"card-tile-bulge"}>
                    <div className={"card-tier-tile"}
                         title={"Your level of knowledge of this card."}>
                        <div className={"valve-effect-wrapper card-level-valve-wrapper"}>
                            <FontAwesomeIcon icon={faSignal}
                                             className={"valve-effect-object card-level"}/>
                            <div className={"valve card-level-valve"} ref={cardTierValve}/>
                        </div>
                    </div>
                </div>
                <textarea ref={questionField} id={"question"} rows="1" wrap="soft"
                          className={"textarea card-tile-textarea"}
                          defaultValue={props.cardState.question}
                          onClick={setEditMode} onSelect={setEditMode}
                          onInput={resetFieldValidity} maxLength={MAX_QUESTION_LENGTH}/>
                <textarea ref={answerField} id={"answer"} rows="1" wrap="soft"
                          className={"textarea card-tile-textarea"}
                          defaultValue={props.cardState.answer}
                          onClick={setEditMode} onSelect={setEditMode}
                          onInput={resetFieldValidity} maxLength={MAX_ANSWER_LENGTH}/>
                <div ref={editButtons} className={"add-new-card-buttons"}>
                    <button id={"success"} className={"btn"} onClick={QAChangeWrapper}>
                        <FontAwesomeIcon icon={faCheck}/>
                    </button>
                    <br/>
                    <button id={"danger"} className={"btn"} onClick={revertChanges}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </button>
                </div>
                <div ref={deleteCardButton} className={"delete-card-button-wrapper"}>
                    <button id={"danger"} className={"btn delete-card-button"}
                            onClick={delOverlayWrapper}>
                        <FontAwesomeIcon icon={faTrash}/>
                    </button>
                </div>
            </div>
        </Fragment>
    )
}

export default forwardRef(CardTile);