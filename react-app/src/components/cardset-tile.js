import React, {
    forwardRef,
    useEffect,
    useImperativeHandle,
    useLayoutEffect,
    useRef
} from 'react'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faGraduationCap,
    faEdit,
    faTrash,
    faBars,
    faSave,
    faUndoAlt,
    faChartLine
} from "@fortawesome/free-solid-svg-icons";
import CtxMenu from "./ctx-menu";
import {useHistory} from "react-router-dom";
import {MAX_NAME_LENGTH} from "../util/constants/cardset-constants";

function CardsetTile(props, ref) {
    let isActive = false;

    const nameTextarea = useRef();
    const nameButtons = useRef();
    const ctxMenuButton = useRef();
    const manipCardsetButtonsWrapper = useRef();
    const learnButtonWrapper = useRef();
    const cardsetTile = useRef();

    const history = useHistory();

    useImperativeHandle(ref, () => ({
        selectNewCardsetTileTitle: () => nameTextarea.current.select(),
        isActive: () => {
            return isActive
        },
        enableCardsetTile: enableCardsetTile,
        disableCardsetTile: disableCardsetTile
    }));

    useEffect(() => {
        if (!props.cardsetState.numOfCards) {
            const learnButton = learnButtonWrapper.current.children[0];
            if (learnButton.classList.contains("primary-btn")) {
                learnButton.classList.remove("primary-btn");
            }
            learnButton.classList.add("secondary-btn", "disabled");
            learnButtonWrapper.current.title = "No cards for learning.";
        }
    })

    useLayoutEffect(() => {
        resizeTextArea();
    });

    const delOverlayWrapper = () => {
        props.onDelOverlay(props.cardsetState.getCopy());
    }

    const statOverlayWrapper = () => {
        props.onStatOverlay(props.cardsetState.id);
    };

    const learnCardset = () => {
        history.push("/cardsets/" + props.cardsetState.id + "/learn")
    }

    const editCardset = () => {
        history.push("/cardsets/" + props.cardsetState.id + "/cards");
    }

    const setName = (name) => {
        nameTextarea.current.value = name;
    }

    const enableCardsetTile = () => {
        cardsetTile.current.style.pointerEvents = null;
    }

    const disableCardsetTile = () => {
        cardsetTile.current.style.pointerEvents = "none";
    }

    const activateCardsetTile = () => {
        nameTextarea.current.style.outlineStyle = "auto";
        manipCardsetButtonsWrapper.current.style.visibility = "hidden";
        ctxMenuButton.current.style.visibility = "hidden";
    }

    const deactivateCardsetTile = () => {
        nameTextarea.current.style.outlineStyle = null;
        manipCardsetButtonsWrapper.current.style.visibility = null;
        ctxMenuButton.current.style.visibility = null;
    }

    const setVisibilityForNameEditButtons = () => {
        nameButtons.current.style.visibility = "visible";
        nameButtons.current.style.opacity = "1";
    }

    const removeVisibilityForNameEditButtons = () => {
        nameButtons.current.style.visibility = "hidden";
        nameButtons.current.style.opacity = "0";
    }

    const enableRenameMode = () => {
        if (nameButtons.current.style.visibility === "visible") {
            return;
        }
        setVisibilityForNameEditButtons();
        props.renameCardsetFuncs.disableElementsVisibleForRenameCardsetMode();
        isActive = true;
        props.renameCardsetFuncs.disableNonActiveCardsetTiles();
        activateCardsetTile();
    }

    const disableRenameMode = () => {
        removeVisibilityForNameEditButtons();
        props.renameCardsetFuncs.enableElementsVisibleForRenameCardsetMode();
        props.renameCardsetFuncs.enableNonActiveTiles();
        isActive = false;
        deactivateCardsetTile();
    }

    const setNormalMode = () => {
        resizeTextArea();
        disableRenameMode();
        props.checkSearchQueryEqualsAfterRename();
    };

    const saveNewName = () => {
        const newName = nameTextarea.current.value;
        props.onRename(props.cardsetState.id, newName, setNormalMode, reportNameValidity);
    };

    const returnOldNameWrapper = () => {
        props.onOldNameReturn(props.cardsetState.id, setName);
        setNormalMode();
        reportNameValidity("");
    };

    const reportNameValidity = (errMsg) => {
        if (errMsg) {
            nameTextarea.current.style.outlineColor = "#E41E65";
            nameTextarea.current.style.outlineStyle = "auto";
        }
        nameTextarea.current.setCustomValidity(errMsg);
        nameTextarea.current.reportValidity();
    }

    const resizeTextArea = () => {
        nameTextarea.current.style.outline = null;
        nameTextarea.current.style.height = "auto";
        nameTextarea.current.style.height = (nameTextarea.current.scrollHeight) + "px";
    }

    const ctxMenuButtons = [
        <button onClick={statOverlayWrapper}>
            <FontAwesomeIcon icon={faChartLine}/> Statistics
        </button>,
        <button onClick={delOverlayWrapper}>
            <FontAwesomeIcon icon={faTrash}/> Delete
        </button>
    ];

    return (
        <div ref={cardsetTile} className={"cardset-tile-box"}>
            <div className={"cardset-tile"}>
                <div ref={ctxMenuButton}>
                    <button className={"btn more-btn hover-btn"}>
                        <FontAwesomeIcon icon={faBars}/>
                    </button>
                    <CtxMenu buttons={ctxMenuButtons}/>
                </div>
                <div className={"cardset-tile-header"}>
                    <div className={"cardset-tile-num-of-cards"}>
                        Cards<br/>
                        <b>{props.cardsetState.numOfCards}</b>
                    </div>
                    <div className={"cardset-tile-progress"}>
                        Progress<br/>
                        <b>{props.cardsetState.progress}%</b>
                    </div>
                </div>
                <div className={"cardset-tile-title-box"}>
                    <textarea ref={nameTextarea}
                              rows={1} wrap="soft"
                              maxLength={MAX_NAME_LENGTH}
                              onInput={() => {
                                  reportNameValidity("");
                                  resizeTextArea();
                              }}
                              onFocus={enableRenameMode}
                              title={"Click to edit the title"}
                              className={"cardset-tile-title"}
                              defaultValue={props.cardsetState.name}>
                    </textarea>
                    <div className={"cardset-tile-title-buttons-box"}>
                        <div ref={nameButtons} className={"cardset-tile-title-buttons"}>
                            <button id={"title-cancel-btn"} className={"btn primary-btn"}
                                    onClick={returnOldNameWrapper}>
                                <FontAwesomeIcon icon={faUndoAlt}/> Cancel
                            </button>
                            <button id={"title-save-btn"} className={"btn primary-btn"}
                                    onClick={saveNewName}>
                                <FontAwesomeIcon icon={faSave}/> Save
                            </button>
                        </div>
                    </div>
                </div>
                <div className={"cardset-tile-buttons"}>
                    <div ref={manipCardsetButtonsWrapper}>
                        <div ref={learnButtonWrapper}>
                            <button id={"cardset-tile-learn-button"}
                                    className={"btn primary-btn hover-btn"} onClick={learnCardset}>
                                <FontAwesomeIcon icon={faGraduationCap}/> Learn
                            </button>
                        </div>
                        <button id={"cardset-tile-edit-button"}
                                className={"btn primary-btn hover-btn"} onClick={editCardset}>
                            <FontAwesomeIcon icon={faEdit}/> Edit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default forwardRef(CardsetTile);
