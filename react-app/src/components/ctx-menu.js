import React from 'react'

function CtxMenu(props) {
    const buttons = props.buttons.map((button) =>
        <li>{button}</li>
    );
    return (
        <div className={"ctx-menu"}>
            <ul>
                {buttons}
            </ul>
        </div>
    );
}

export default (CtxMenu);