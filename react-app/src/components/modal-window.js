import {forwardRef, useImperativeHandle, useRef, useState} from "react";

function ModalWindow(props, ref) {
    const [title, setTitle] = useState();
    const [body, setBody] = useState();
    const [buttons, setButtons] = useState();
    const overlay = useRef();

    useImperativeHandle(ref, () => ({
        switchOverlay: () => switchOverlay(),
        setModalWindowContent: (title, body, buttons) => {
            setTitle(title);
            setBody(body);
            setButtons(buttons);
        }
    }));

    const switchOverlay = () => {
        if (overlay.current.style.visibility === 'visible') {
            overlay.current.style = null;
        } else {
            overlay.current.style.visibility = 'visible';
            overlay.current.style.opacity = '1';
            overlay.current.style.transition = 'all 0.2s ease-out 0s';
        }
    }
    return (
        <div ref={overlay} id="overlay">
            <div className="popup">
                <p className="zag">{title}</p>
                <div className={"overlay-body"}>
                    {body}
                </div>
                <div className={"overlay-buttons"}>
                    {buttons}
                </div>
            </div>
        </div>
    );
}

export default forwardRef(ModalWindow);