const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const staticResources = '../src/main/resources/static';
const templates = '../templates';

module.exports = (env, options) => {
    return {
        // Where files should be sent once they are bundled
        output: {
            path: path.join(__dirname, staticResources),
            filename: 'index.bundle.js',
            publicPath: '/',
        },
        // webpack 5 comes with devServer which loads in development mode
        devServer: {
            port: 3000,
            historyApiFallback: true,
            proxy: {
                '/api': 'http://localhost:8080',
            },
            //watchContentBase: true
        },
        // Rules of how webpack will take our files, complie & bundle them for the browser
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /nodeModules/,
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, 'css-loader']
                }
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './public/index.html',
                filename: options.mode === 'development' ? 'index.html' : `${templates}/index.html`,
                favicon: './public/logo-white.ico'
            }),
            new MiniCssExtractPlugin()
        ],
    }
}

