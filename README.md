# Flashcards
Our web app implements the Leitner system is a widely used memorizing technique that using flashcards

## Stack
Backend: Spring, PostgreSQL

Frontend: React

## Screenshost

| Start Page | Learn Page |
| ---------- | ---------- |
| ![Flashcards start page](/uploads/b5c3221c1be3ba89b6c30db65fad09e3/изображение.png) | ![Flashcards learn page](/uploads/7f620a965f9ba9b581d59519fea8f295/изображение.png) |
